switch (mpos)
{
	case 0: break;
	
	case 1: 
	{
		room_goto(rm_level1);
		break;
	}
	
	case 2: 
	{
		scr_load();
		break;
	}
	
	case 3: 
	{
		if (room != rm_menu) {
			scr_save()
		}
		else game_end();
		break;
	}

	case 4: 
	{
		if (room != rm_menu) {
			if (global.colorPalette == 0) {
				global.colorPalette = 1;
			}
			else if (global.colorPalette == 1) {
				global.colorPalette = 0;
			}
		}
		break;
	}
	
	case 5: 
	{
		if (room != rm_menu) {
			game_end();
		}
		break;
	}
	
	case 6: break;
	
	case 7: break;
	
	default: break;
}
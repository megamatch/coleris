/// @description cameraControl(x, y)
/// @function Sets camera position relative to x and y
/// @param x
/// @param y

camera_set_view_pos(view_camera[0], argument0 - obj_cameraControl.horizontalCameraSize/2, argument1 - obj_cameraControl.verticalCameraSize/2);
camera_set_view_size(view_camera[0], obj_cameraControl.horizontalCameraSize, obj_cameraControl.verticalCameraSize);
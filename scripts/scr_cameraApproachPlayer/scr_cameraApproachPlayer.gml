
var camX, camY, approachSpeed, finalX, finalY;

camX = camera_get_view_x(view_camera[0]);
camY = camera_get_view_y(view_camera[0]);

if (variable_global_exists("roomStartCameraMovement")) {
	if (global.roomStartCameraMovement == true)
		approachSpeed = .06;
		
	else {
		approachSpeed = .125;
	}
}

else {
	approachSpeed = .10;
}

finalX = lerp(camX, obj_player.x - obj_cameraControl.horizontalCameraSize/2, approachSpeed);
finalY = lerp(camY, obj_player.y - obj_cameraControl.verticalCameraSize/2, approachSpeed);

camera_set_view_pos(view_camera[0], finalX, finalY);
camera_set_view_size(view_camera[0], obj_cameraControl.horizontalCameraSize, obj_cameraControl.verticalCameraSize);
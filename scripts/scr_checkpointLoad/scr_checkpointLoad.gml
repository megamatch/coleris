if (file_exists("checkpoint.sav"))
{
	var loadFile = file_text_open_read("checkpoint.sav");
	var playerX = file_text_read_real(loadFile);
	file_text_readln(loadFile);
	var playerY = file_text_read_real(loadFile)
	file_text_close(loadFile);
	
	instance_destroy(obj_player, false);
	instance_create_depth(playerX, playerY, 1, obj_player);
}

else 
{
	//nothing
}
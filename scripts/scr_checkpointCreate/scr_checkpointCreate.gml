if (file_exists("checkpoint.sav")) 
{
	file_delete("checkpoint.sav");
}
var checkpointFile = file_text_open_write("checkpoint.sav");
var playerLocationX = obj_player.x;
var playerLocationY = obj_player.y;
file_text_write_real(checkpointFile, playerLocationX);
file_text_writeln(checkpointFile);
file_text_write_real(checkpointFile, playerLocationY);
file_text_close(checkpointFile);
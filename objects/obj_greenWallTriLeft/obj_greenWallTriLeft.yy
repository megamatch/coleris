{
    "id": "3ae259f7-1d6b-438e-b8be-3c5424ddcfc0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greenWallTriLeft",
    "eventList": [
        {
            "id": "defe14f7-e200-4139-b9a5-e981717b7a95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3ae259f7-1d6b-438e-b8be-3c5424ddcfc0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "843b1938-27ab-4c3a-8be9-8bf85da4f029",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "0eb66cb2-44e3-49c3-9557-41ee8d250f6d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "82476345-9f30-4668-abe5-490153ebd5ec",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 128
        },
        {
            "id": "6f5ce30e-1f3a-4fee-9a8d-c7d3e143d58d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1554438e-1095-4ace-95f3-f37e24eb4dd2",
    "visible": true
}
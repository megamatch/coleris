
// Makes sure all physics objects are active
obj_allParent.phy_active = true;

depth = 1;

// If global variables don't exist, make them
if (!variable_global_exists("paletteTest"))
{
	global.paletteTest = 0;
}

if (!variable_global_exists("colorPalette"))
{
	global.colorPalette = 0;
}

if (!variable_global_exists("colorShift"))
{
	global.colorShift = 0;
}

// These two statements remember the color of the player and setup physics accordingly
if (global.colorShift = 1)
{
	sprite_index = spr_playerBlue;
	obj_collisionBlue.phy_active = false;
	obj_collisionGreen.phy_active = true;
}

else if (global.colorShift = 0)
{
	sprite_index = spr_playerGreen;
	obj_collisionBlue.phy_active = true;
	obj_collisionGreen.phy_active = false;
}

// Sets the menu variable
menuOpen = false;

deathEffect = false;
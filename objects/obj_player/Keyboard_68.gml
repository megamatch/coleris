/// @description Torque Right
// You can write your code in this editor
if (!keyboard_check(vk_right))
{
	physics_apply_torque(torquePower);
	physics_apply_impulse(x + 1, y, moveImpulse, 0);
}
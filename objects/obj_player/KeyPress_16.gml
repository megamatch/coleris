/// @description Color Shift

if (global.colorShift = 0 and physics_test_overlap(x,y,0,obj_collisionGreen) == false and deathEffect == false)
{
	sprite_index = spr_playerBlue;
	global.colorShift = 1;
	obj_collisionBlue.phy_active = false;
	obj_collisionGreen.phy_active = true;
	audio_play_sound(snd_shift, 10, false);
	part_particles_create(global.particleSystem1, x, y, global.shiftParticle, 1);
	
	if (boostShift > 0) {
		physics_apply_impulse(phy_position_xprevious, phy_position_yprevious, sign(phy_speed_x) * jumpPower/4, sign(phy_speed_y) * jumpPower/4);
		boostShift--;
	}
}

else if (global.colorShift = 1 and physics_test_overlap(x,y,0,obj_collisionBlue) == false and deathEffect == false)
{
	sprite_index = spr_playerGreen;
	global.colorShift = 0;
	obj_collisionBlue.phy_active = true;
	obj_collisionGreen.phy_active = false;
	audio_play_sound(snd_shift, 10, false);
	part_particles_create(global.particleSystem1, x, y, global.shiftParticle, 1);
	
	if (boostShift > 0) {
		physics_apply_impulse(phy_position_xprevious, phy_position_yprevious, sign(phy_speed_x) * jumpPower/4, sign(phy_speed_y) * jumpPower/4);
		boostShift--;
	}

}

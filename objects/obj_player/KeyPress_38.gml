/// @description Jump

if (deathEffect == false and jumpPossible > 0 and !instance_exists(obj_menu) and !keyboard_check_pressed(vk_space) and !keyboard_check_pressed(ord("W")))
{
	audio_play_sound(snd_jump, 9, false);
	physics_apply_impulse(x, yCollisionPoint, 0, -jumpPower);
}


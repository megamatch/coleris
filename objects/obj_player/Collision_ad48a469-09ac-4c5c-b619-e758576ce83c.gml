phy_active = false;

alarm_set(0,1.5*room_speed);

image_alpha = 0;

deathEffect = true;

if (global.colorShift == 0 and global.colorPalette == 0) {
	effect_create_above(ef_firework, x, y, 5, global.greenParticleColor);
}
else if (global.colorShift == 1 and global.colorPalette == 0) {
	effect_create_above(ef_firework, x, y, 5, global.blueParticleColor);
}
	
if (global.colorShift == 0 and global.colorPalette == 1) {
	effect_create_above(ef_firework, x, y, 5, global.orangeParticleColor);
}
else if (global.colorShift == 1 and global.colorPalette == 1) {
	effect_create_above(ef_firework, x, y, 5, global.fuschiaParticleColor);
}

audio_play_sound(snd_impact, 10, false);
effect_create_above(ef_smoke, x, y + sprite_height/2, 10, c_dkgray);

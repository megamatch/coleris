if (global.colorPalette == 1 and sprite_index == spr_playerBlue and deathEffect == false)
{
	draw_sprite_ext(spr_playerFuschia, 0, x, y, image_xscale, image_yscale, -phy_rotation, c_white, 1);
}

else if (global.colorPalette == 1 and sprite_index == spr_playerGreen and deathEffect == false)
{
	draw_sprite_ext(spr_playerOrange, 0, x, y, image_xscale, image_yscale, -phy_rotation, c_white, 1);
}

else if (global.colorPalette == 0)
{
	draw_self();
}

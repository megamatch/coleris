/// @description Step Events

// Camera movement
scr_cameraApproachPlayer();

// Determine roll particle direction
particleDirection = point_direction(phy_position_x, phy_position_y, phy_position_xprevious, phy_position_yprevious);

// Store physics speed as a variable
if (phy_active)
	physicsSpeed = phy_speed;

// Jump control
if (!physics_test_overlap(x, y + 6, 0, obj_collisionParent))
{
	jumpPossible = 0;
	audio_stop_sound(snd_roll);
}

// Set up rolling particles
part_type_shape(global.rollParticle, pt_shape_pixel);

if (global.colorShift == 0 and global.colorPalette == 0) {
	part_type_color1(global.rollParticle, global.greenParticleColor);
	part_type_color1(global.shiftParticle, global.blueParticleColor);
}
else if (global.colorShift == 1 and global.colorPalette == 0) {
	part_type_color1(global.rollParticle, global.blueParticleColor);
	part_type_color1(global.shiftParticle, global.greenParticleColor);
}
	
if (global.colorShift == 0 and global.colorPalette == 1) {
	part_type_color1(global.rollParticle, global.orangeParticleColor);
	part_type_color1(global.shiftParticle, global.fuschiaParticleColor);
}
else if (global.colorShift == 1 and global.colorPalette == 1) {
	part_type_color1(global.rollParticle, global.fuschiaParticleColor);
	part_type_color1(global.shiftParticle, global.orangeParticleColor);
}

part_type_life(global.rollParticle, room_speed/10, room_speed/2);

part_type_speed(global.rollParticle, phy_speed * .4, phy_speed * .8, .01, 0);

part_type_scale(global.rollParticle, 1, 1);

part_type_size(global.rollParticle, 1, 4, 0, 0);

part_type_alpha1(global.rollParticle, .7);

part_type_alpha1(global.shiftParticle, .5);

part_type_blend(global.rollParticle, false);

part_type_size(global.shiftParticle, 1.3, 1.3, .2, 0);

// Create roll particles
if (physics_test_overlap(x, y + 6, 0, obj_collisionParent) and phy_position_x > phy_position_xprevious and yCollisionPoint > y)
{
	part_type_direction(global.rollParticle, particleDirection - 30, particleDirection, 0, 0);
}

else if (physics_test_overlap(x, y + 6, 0, obj_collisionParent) and phy_position_x < phy_position_xprevious and yCollisionPoint > y)
{
	part_type_direction(global.rollParticle, particleDirection, particleDirection + 30, 0, 0);
}

// Impact effects and sound control, predicts impacts based on physics speed and colorShift value
if (deathEffect == false and phy_speed > 10 and sign(phy_speed_y) == 1 and impactPlayable and jumpPossible = 0 and physics_test_overlap(x + phy_speed_x, y + abs(phy_speed_y), 0, obj_collisionParent)) {
	if (global.colorShift == 0 and physics_test_overlap(x + phy_speed_x, y + abs(phy_speed_y), 0, obj_collisionBlue)) {
		audio_play_sound(snd_impact, 10, false);
		impactPlayable = false;
		alarm_set(1, room_speed);
		effect_create_above(ef_smoke, x, y + sprite_height/2, 10, c_dkgray);
	}
	else if (global.colorShift == 1 and physics_test_overlap(x + phy_speed_x, y + abs(phy_speed_y), 0, obj_collisionGreen)) {
		audio_play_sound(snd_impact, 10, false);
		impactPlayable = false;
		alarm_set(1, room_speed);
		effect_create_above(ef_smoke, x, y + sprite_height/2, 10, c_dkgray);
	}
	else if (global.colorShift == 0 and !physics_test_overlap(x + phy_speed_x, y + abs(phy_speed_y), 0, obj_collisionGreen)) {
		audio_play_sound(snd_impact, 10, false);
		impactPlayable = false;
		alarm_set(1, room_speed);
		effect_create_above(ef_smoke, x, y + sprite_height/2, 10, c_dkgray);
	}
	else if (global.colorShift == 1 and !physics_test_overlap(x + phy_speed_x, y + abs(phy_speed_y), 0, obj_collisionBlue)) {
		audio_play_sound(snd_impact, 10, false);
		impactPlayable = false;
		alarm_set(1, room_speed);
		effect_create_above(ef_smoke, x, y + sprite_height/2, 10, c_dkgray);
	}
}

if (!phy_active)
	audio_stop_sound(snd_roll);
	
// Update audio listener position
audio_listener_position(x, y, 0);
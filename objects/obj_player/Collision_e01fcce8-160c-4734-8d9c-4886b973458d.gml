/// @description Environmental Collision

// Jump control
if (phy_collision_y > y + 34 and jumpPossible < jumpMaximum)
{
	jumpPossible = jumpMaximum;
	boostShift = boostShiftMax;
}

// Store last collision points as variables so other scripts can use them
yCollisionPoint = phy_collision_y;
xCollisionPoint = phy_collision_x;


// Rolling particle effects
if (phy_speed > 1 and phy_collision_y > y + 16)
	part_particles_create(global.particleSystem1, xCollisionPoint, yCollisionPoint + 2, global.rollParticle, 8);
	
else if (phy_speed > 1 and phy_collision_y < y + 16)
	part_particles_create(global.particleSystem1, xCollisionPoint, yCollisionPoint, global.rollParticle, 8);

if (!audio_is_playing(snd_roll)) {
	if (physics_test_overlap(phy_collision_x + (sign(phy_collision_x) * 32), phy_collision_y, 0, obj_collisionBlue) and global.colorShift == 0) {
		audio_play_sound(snd_roll, 10, true);
		audio_sound_gain(snd_roll, physicsSpeed * .06, 0);
	}
	else if (physics_test_overlap(phy_collision_x + (sign(phy_collision_x) * 32), phy_collision_y, 0, obj_collisionGreen) and global.colorShift == 1) {
		audio_play_sound(snd_roll, 10, true);
		audio_sound_gain(snd_roll, physicsSpeed * .06, 0);
	}
	else if (physics_test_overlap(phy_collision_x + (sign(phy_collision_x) * 32), phy_collision_y, 0, obj_normalWallParent)) {
		audio_play_sound(snd_roll, 10, true);
		audio_sound_gain(snd_roll, physicsSpeed * .06, 0);
	}
}

if (audio_is_playing(snd_roll)) {
	if (physics_test_overlap(phy_collision_x + (sign(phy_collision_x) * 32), phy_collision_y, 0, obj_collisionBlue) and global.colorShift == 0) {
		audio_sound_gain(snd_roll, physicsSpeed * .06, 0);
	}
	else if (physics_test_overlap(phy_collision_x + (sign(phy_collision_x) * 32), phy_collision_y, 0, obj_collisionGreen) and global.colorShift == 1) {
		audio_sound_gain(snd_roll, physicsSpeed * .06, 0);
	}
	else if (physics_test_overlap(phy_collision_x + (sign(phy_collision_x) * 32), phy_collision_y, 0, obj_normalWallParent)) {
		audio_sound_gain(snd_roll, physicsSpeed * .06, 0);
	}
	else
		audio_sound_gain(snd_roll, 0, 0);
}
/// @description Start Music
// You can write your code in this editor

if (room == rm_menu or room == rm_level1) {
	audio_play_sound(snd_loop1, 10, true);
}

else if (room == rm_level2 or room == rm_level3) {
	audio_play_sound(snd_loop2, 10, true);
}

else if (room == rm_level4) {
	audio_play_sound(snd_loop3, 10, true);
}

else if (room == rm_level5) {
	audio_play_sound(snd_loop4, 10, true);
}

else if (room == rm_level6 or room == rm_level7) {
	audio_play_sound(snd_loop5, 10, true);
}

else if (room == rm_final) {
	audio_play_sound(snd_loop6, 10, true);
}
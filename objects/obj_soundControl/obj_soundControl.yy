{
    "id": "34d2bfc2-e303-41a8-9761-7a27f024e56e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_soundControl",
    "eventList": [
        {
            "id": "9cb1dda7-30a3-479f-af99-86778f888424",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 7,
            "m_owner": "34d2bfc2-e303-41a8-9761-7a27f024e56e"
        },
        {
            "id": "be9b5801-5b2b-434a-8254-a8500a150fd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "34d2bfc2-e303-41a8-9761-7a27f024e56e"
        },
        {
            "id": "978d186a-344c-4afd-87cf-4d8096cf8967",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "34d2bfc2-e303-41a8-9761-7a27f024e56e"
        },
        {
            "id": "b2be3523-444a-4afd-8c06-f0fb1d952c7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "34d2bfc2-e303-41a8-9761-7a27f024e56e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}
{
    "id": "a69cdf29-4383-4ba1-b52e-7cb62051f4c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greyWallSquare",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5b5e3d7d-e290-412f-af09-7d90e9372dd4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "286ea624-7475-49d5-b3b0-ee87f7efa2cc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "6ebd2a2f-7e07-4c35-8b23-ddfd34c44781",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 0
        },
        {
            "id": "4613bbf6-2b5d-4e6a-9610-be04d0e1788d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 128
        },
        {
            "id": "bddf2c4e-97b6-45b3-9ca7-971edf74589f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dcc4f1c8-da59-4c07-ab39-f70a98347e6a",
    "visible": true
}
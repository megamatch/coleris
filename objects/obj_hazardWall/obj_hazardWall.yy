{
    "id": "3d974266-11c7-474b-adaa-0ed6d69af3fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hazardWall",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "aaecb93d-ea98-42d2-92a4-286de9afe841",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "bcbc5b54-7eb9-4243-9a75-1f81ff2a438f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "ed1943d6-6761-45ca-9ffa-ce0013051d62",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 257,
            "y": 0
        },
        {
            "id": "7d293343-0432-4bd7-8bea-a694eb043056",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 257,
            "y": 129
        },
        {
            "id": "cdef45da-2a0e-48e3-bdde-122a884e7f6b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 129
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "189c9569-9674-4ffd-a6ce-088a42024e5e",
    "visible": true
}
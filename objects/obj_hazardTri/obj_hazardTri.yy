{
    "id": "9740323b-c9bf-4302-a17b-cc478188428d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hazardTri",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "aaecb93d-ea98-42d2-92a4-286de9afe841",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "18fd72ef-4783-4058-91e8-80886904cb87",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "53357de1-9379-4962-843c-5af03f9da4f7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 128
        },
        {
            "id": "a91d3e0b-aa3f-40b7-95ce-b11ff8283504",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3e5b5609-ddd8-4437-9ea8-42ccb62a6c35",
    "visible": true
}
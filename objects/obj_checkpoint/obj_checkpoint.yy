{
    "id": "8620e052-f2c3-427b-b395-fd0d73e22e95",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_checkpoint",
    "eventList": [
        {
            "id": "e0e9c86f-eb9d-43cd-8918-659ecf1e4e38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1b6ac3b7-425e-4ea0-b7c4-baee5a0a8dc3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8620e052-f2c3-427b-b395-fd0d73e22e95"
        },
        {
            "id": "e3859e20-08c4-4499-8f2a-c9f86384d2a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8620e052-f2c3-427b-b395-fd0d73e22e95"
        },
        {
            "id": "063f2165-e688-4cbc-988a-7bb26cc0cc2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8620e052-f2c3-427b-b395-fd0d73e22e95"
        }
    ],
    "maskSpriteId": "b3e3ac4f-f649-4e9d-9423-2f19bf5f9c65",
    "overriddenProperties": null,
    "parentObjectId": "ebbfd35c-fff3-4b64-aa98-8c6e356afae3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": true,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "8610176a-5999-423d-acfd-ad14e1d273e5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "af4dea6e-2de3-4ad5-b1cb-cefc904da222",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "8a3d4a76-05fc-410d-87ff-2fa504b2b874",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "590e5650-f7c0-4151-98ab-df0894c12f78",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "ede2302e-2780-478e-825e-10a4e670b14a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "checkpointColor",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "bdefaaa9-9b90-439b-8d3e-5974499af10b",
    "visible": true
}
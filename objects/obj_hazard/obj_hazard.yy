{
    "id": "2a28350b-8777-431a-b181-4161217e86c7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hazard",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "aaecb93d-ea98-42d2-92a4-286de9afe841",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "1f3483c3-7afc-4abb-9c32-7d628f2722de",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 0
        },
        {
            "id": "5067b2ee-08eb-4ea4-a138-3b197c2edb7f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 128
        },
        {
            "id": "bf3fb970-919c-4765-bd92-353ec67b8e90",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "16c6b90e-4088-4fa8-915e-3f2dd9569cf2",
    "visible": true
}
/// @description Player hits star

obj_allParent.phy_active = false;
part_particles_create(global.particleSystem1, x, y, global.starParticle, 1);
audio_stop_all();
audio_play_sound(snd_victory, 10, false);
alarm_set(0,2.5*room_speed);
victory = true;
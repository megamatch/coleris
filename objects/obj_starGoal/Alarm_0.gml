/// @description Finish level

if (room_get_name(room) != "rm_final")
{
	room_goto_next();
}

else room_goto(rm_level1);
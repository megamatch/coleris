{
    "id": "4bd726fe-678f-4338-8fe6-075e51cb370f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_starGoal",
    "eventList": [
        {
            "id": "b57fa040-2119-46eb-a803-ad5604770aea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1b6ac3b7-425e-4ea0-b7c4-baee5a0a8dc3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4bd726fe-678f-4338-8fe6-075e51cb370f"
        },
        {
            "id": "ae0b2ac4-b637-437d-9889-3210f60ad1f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4bd726fe-678f-4338-8fe6-075e51cb370f"
        },
        {
            "id": "31b37eec-4484-4636-91b4-bc93ba27ea64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4bd726fe-678f-4338-8fe6-075e51cb370f"
        },
        {
            "id": "7ad5b425-cfb5-4bdd-b706-16e88fa2584c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "4bd726fe-678f-4338-8fe6-075e51cb370f"
        },
        {
            "id": "084becbf-f115-489c-8223-4dc7a8788011",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4bd726fe-678f-4338-8fe6-075e51cb370f"
        },
        {
            "id": "5cb69085-c1a2-4e1f-8699-fc2a4bc71e8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4bd726fe-678f-4338-8fe6-075e51cb370f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ebbfd35c-fff3-4b64-aa98-8c6e356afae3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": true,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "62c72d32-b3b6-41a0-a64a-7cdd2aacc59f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 40
        },
        {
            "id": "eb8dcbf8-3d33-4299-b2f3-e01caa5b73d4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 95,
            "y": 40
        },
        {
            "id": "f54fea43-6611-4558-be00-92f60f50c50a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 95,
            "y": 99
        },
        {
            "id": "faa4f5a5-0dfc-4847-9937-717636bee8cf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 99
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b3e3ac4f-f649-4e9d-9423-2f19bf5f9c65",
    "visible": true
}
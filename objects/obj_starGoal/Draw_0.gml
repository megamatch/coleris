


if (victory == true) {
	image_alpha = clamp(image_alpha - 0.02, 0, 1);
	image_xscale = clamp(image_xscale - 0.05, 0, 1);
	image_yscale = clamp(image_yscale - 0.05, 0, 1);
}

else image_alpha = 1;

draw_self();
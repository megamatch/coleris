var move = 0;
move -= max(keyboard_check_pressed(ord("W")), keyboard_check_pressed(vk_up), 0);
move += max(keyboard_check_pressed(ord("S")), keyboard_check_pressed(vk_down), 0);

if (move != 0)
{
	mpos += move;
	if (mpos < 1 and room != rm_menu) mpos = array_length_1d(menu) - 3;
	if (room == rm_menu and mpos < 1) mpos = array_length_1d(menu) - 5;
	if (mpos > array_length_1d(menu) - 3 and room != rm_menu) mpos = 1;
	if (room == rm_menu and mpos > array_length_1d(menu) - 5) mpos = 1;
}

if (mpos == 0) mpos = 1;

var push;
push = max(keyboard_check_released(vk_enter), keyboard_check_released(vk_space));
if (push == 1) scr_menu();
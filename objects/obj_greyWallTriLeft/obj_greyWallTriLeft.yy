{
    "id": "ebadb76c-794b-4c3f-9b45-8ac71004791b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greyWallTriLeft",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5b5e3d7d-e290-412f-af09-7d90e9372dd4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "d6d3a912-8bc0-4009-84fc-6efb9b080327",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "cd63cd70-9c42-4505-92bb-aa24dc78aed0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 128
        },
        {
            "id": "526b7940-f569-499d-9548-7ab69ac018d5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "21321d64-6a02-4009-aa10-742b82b08948",
    "visible": true
}
{
    "id": "3a012f8c-819a-423f-8cec-8f2cf66ba819",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greenWallSquare",
    "eventList": [
        {
            "id": "d1e0c7fd-5332-4a67-aee7-0ffc419f5dd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3a012f8c-819a-423f-8cec-8f2cf66ba819"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "843b1938-27ab-4c3a-8be9-8bf85da4f029",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "1ce4d5bc-d7cb-474f-8132-8116a7909b90",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "9c7ab650-4b2e-4105-a1f9-0656d0cfa0b1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 0
        },
        {
            "id": "b2268996-cae5-47d2-ab4a-f049e1d34302",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 128
        },
        {
            "id": "4cc90dab-ad93-43d4-8553-aa90755b5f05",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "57344181-8f57-42ed-a472-ae21c40fdf07",
    "visible": true
}
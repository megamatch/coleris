{
    "id": "4ea74e93-8d50-4135-84f4-ace245c33962",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blueWallTriLeft",
    "eventList": [
        {
            "id": "1b715828-79db-4f7b-8d10-c815494f9c96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4ea74e93-8d50-4135-84f4-ace245c33962"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "85e056ee-c1cf-41a5-bbba-fff222068784",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "d10974c7-2d1e-49af-9e3d-0abb5850cc5b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "64576fa1-bb74-4cfa-b50f-e9ff1069038a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 128
        },
        {
            "id": "1628ffed-4738-474a-b891-f6b1309c5d38",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e704c05-1f91-4a1d-9323-f390b3189d6e",
    "visible": true
}
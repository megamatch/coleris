{
    "id": "e0a63d8a-a7d7-4bfe-af70-1d0130fa9cc9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hazardWallRotating",
    "eventList": [
        {
            "id": "82a58179-c099-4396-8a7c-fa8e2469e409",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0a63d8a-a7d7-4bfe-af70-1d0130fa9cc9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "aaecb93d-ea98-42d2-92a4-286de9afe841",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "0a4b3fed-2dc1-40ca-88e5-317127656b18",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "c4a2cb11-3ee4-44f5-afdb-f32c4739e8b2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 257,
            "y": 0
        },
        {
            "id": "41f581be-30cd-4b47-a6e5-d9818984fb29",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 257,
            "y": 129
        },
        {
            "id": "16ce04e9-8edc-41c6-8b15-a9048390de17",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 129
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "189c9569-9674-4ffd-a6ce-088a42024e5e",
    "visible": true
}
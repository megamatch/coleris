{
    "id": "d85fcda0-ea6a-497d-8537-1ef3e9a12d79",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cameraSensor",
    "eventList": [
        {
            "id": "fb7aab41-c467-4c09-a761-8f4a52c364bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d85fcda0-ea6a-497d-8537-1ef3e9a12d79"
        },
        {
            "id": "d05a2d4d-7a7a-4000-ac36-8f0a9aafa73d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d85fcda0-ea6a-497d-8537-1ef3e9a12d79"
        },
        {
            "id": "a319a27b-382a-437c-82ef-54c9db4ce24c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "d85fcda0-ea6a-497d-8537-1ef3e9a12d79"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": true,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "95406d64-97e9-4d78-8102-f958dd05f4ed",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": -64,
            "y": -64
        },
        {
            "id": "cf1eded9-93c1-4448-9fe8-964eb4eabb9b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 96,
            "y": -64
        },
        {
            "id": "d38298de-81d5-4783-9ddb-b1245409030b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 128
        },
        {
            "id": "55296ea3-1f8e-42ee-bb74-f97815436bc9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": -64,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}
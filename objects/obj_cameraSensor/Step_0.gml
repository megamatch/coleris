	
if (instance_exists(obj_player) == true) {
	x = camera_get_view_x(view_camera[0]) + obj_cameraControl.horizontalCameraSize/2;
	y = camera_get_view_y(view_camera[0]) + obj_cameraControl.verticalCameraSize/2;

	if ((x <= obj_player.x + 1.5 and x >= obj_player.x - 1.5) and (y <= obj_player.y + 1 and y >= obj_player.y - 1) and roomStart == true) {
		obj_player.phy_active = true;
		global.roomStartCameraMovement = false;
		roomStart = false;
	}
}
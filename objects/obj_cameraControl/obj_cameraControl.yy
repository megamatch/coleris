{
    "id": "1586e3b5-c8b6-47f8-b420-e327f613e8be",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cameraControl",
    "eventList": [
        {
            "id": "c0bf0503-09e0-44a8-b5fa-30f9e6ff5071",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1586e3b5-c8b6-47f8-b420-e327f613e8be"
        },
        {
            "id": "eea1298b-2094-4aeb-b5be-69a8f0f619ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1586e3b5-c8b6-47f8-b420-e327f613e8be"
        },
        {
            "id": "82d4332d-8586-4224-9bbf-fa129d1bfcea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "1586e3b5-c8b6-47f8-b420-e327f613e8be"
        },
        {
            "id": "71b71172-d5cd-4be4-a8db-ff41adbfdf19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "1586e3b5-c8b6-47f8-b420-e327f613e8be"
        },
        {
            "id": "7b4bf700-1f1d-4d1f-b4db-e16300cb29dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "1586e3b5-c8b6-47f8-b420-e327f613e8be"
        },
        {
            "id": "1c14e062-a382-4a46-85ad-45d84492ddd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "1586e3b5-c8b6-47f8-b420-e327f613e8be"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}
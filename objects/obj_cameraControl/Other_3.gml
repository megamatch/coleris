/// @description Clean up particles

part_type_destroy(global.rollParticle);

part_type_destroy(global.shiftParticle);

part_type_destroy(global.starParticle);

part_system_destroy(global.particleSystem1);


///View and camera creation

//View size variabes
horizontalCameraSize = 1920;
verticalCameraSize = 1080;

//Enable the use of views
view_enabled = true;

//Make view 0 visible
view_set_visible(0, true);

//Set the port bounds of view 0 to variables
view_set_wport(0, 1920);
view_set_hport(0, 1080);

//Resize and center
window_set_rectangle((display_get_width() - view_wport[0]) * 0.5, (display_get_height() - view_hport[0]) * 0.5, view_wport[0], view_hport[0]);
surface_resize(application_surface,view_wport[0],view_hport[0]);

//Build a camera at (0,0), with size variables, 0 degrees of angle, no target instance, instant-jupming hspeed and vspeed, with a 32 pixel border
camera = camera_create_view(0, 0, horizontalCameraSize, verticalCameraSize, 0, -1, -1, -1, 32, 32);

//Set view0 to use the camera "camera"
view_set_camera(0, camera);

//Basic set up
camera_set_view_pos(view_camera[0], 0, 0);
camera_set_view_size(view_camera[0], horizontalCameraSize, verticalCameraSize);


// Set up particle types
global.greenParticleColor = $429602;
global.blueParticleColor = $cc9606;
global.fuschiaParticleColor = $a8488f;
global.orangeParticleColor = $3d98e8;

global.rollParticle = part_type_create();

part_type_shape(global.rollParticle, pt_shape_pixel);
part_type_color1(global.rollParticle, global.greenParticleColor);
part_type_life(global.rollParticle, room_speed/20, room_speed);
part_type_speed(global.rollParticle, 3, 10, .1, 0);
part_type_direction(global.rollParticle, 180, 180, 0, 0);
part_type_scale(global.rollParticle, 1, 1);
part_type_size(global.rollParticle, 1, 4, 0, 0);

global.shiftParticle = part_type_create();

part_type_shape(global.shiftParticle, pt_shape_ring);
part_type_color1(global.shiftParticle, global.greenParticleColor);
part_type_life(global.shiftParticle, room_speed/6, room_speed/6);
part_type_speed(global.shiftParticle, 0, 0, 0, 0);
part_type_direction(global.shiftParticle, 0, 0, 0, 0);
part_type_scale(global.shiftParticle, 1, 1);
part_system_depth(global.particleSystem1, 2);

global.starParticle = part_type_create();

part_type_shape(global.starParticle, pt_shape_flare);
part_type_color1(global.starParticle, c_orange);
part_type_life(global.starParticle, room_speed/3, room_speed/3);
part_type_speed(global.starParticle, 0, 0, 0, 0);
part_type_direction(global.starParticle, 0, 0, 0, 0);
part_type_scale(global.starParticle, 1, 1);
part_type_size(global.starParticle, .5, 1.5, .1, 0)
part_type_blend(global.starParticle, false);

{
    "id": "ebc396cf-498a-4fc0-9b38-507b8b54a755",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blueWallSquare",
    "eventList": [
        {
            "id": "bf868256-93c0-44ba-b778-324a5938538e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ebc396cf-498a-4fc0-9b38-507b8b54a755"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "85e056ee-c1cf-41a5-bbba-fff222068784",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "224e2073-22c1-460a-b80b-aeab521c670a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "5e4bb9fc-5074-4ee8-b395-df3d05a4a581",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 0
        },
        {
            "id": "80d5fe1d-4bc4-45be-9006-22dd93da2cd2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 128
        },
        {
            "id": "c7018fbc-5466-4d58-9cdc-7fd19c20c055",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "11dc7a69-1780-4655-96df-b3c50b46b178",
    "visible": true
}
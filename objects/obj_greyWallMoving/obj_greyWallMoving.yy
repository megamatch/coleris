{
    "id": "38adfb3d-577b-4598-aa66-461c4d6e4ed0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greyWallMoving",
    "eventList": [
        {
            "id": "0891df0b-0b04-458c-ad86-bf87b2328084",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "38adfb3d-577b-4598-aa66-461c4d6e4ed0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5b5e3d7d-e290-412f-af09-7d90e9372dd4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "a6c1700b-2650-4445-ae5c-9ff050e5d3e2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "0c050ee9-6c00-4768-8823-c812de2f78ee",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 0
        },
        {
            "id": "efd649e2-c5be-4254-ad7a-16cc92d56f47",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 128
        },
        {
            "id": "250600d5-282a-4f3b-a36f-d18b1e3d809d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dcc4f1c8-da59-4c07-ab39-f70a98347e6a",
    "visible": true
}
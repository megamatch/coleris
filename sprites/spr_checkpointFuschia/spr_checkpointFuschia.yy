{
    "id": "c6e2e364-03d6-4838-8e6e-63e744fad803",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_checkpointFuschia",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76ee82f8-7730-457f-b898-05495283709d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6e2e364-03d6-4838-8e6e-63e744fad803",
            "compositeImage": {
                "id": "b4448285-a894-4b96-9a0d-f7d9c0951395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ee82f8-7730-457f-b898-05495283709d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aec1ca10-2782-4497-b1ab-e6057687e35f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ee82f8-7730-457f-b898-05495283709d",
                    "LayerId": "b5fcaf56-c3c6-4a33-8b8a-c5b2cfc45b57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b5fcaf56-c3c6-4a33-8b8a-c5b2cfc45b57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6e2e364-03d6-4838-8e6e-63e744fad803",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
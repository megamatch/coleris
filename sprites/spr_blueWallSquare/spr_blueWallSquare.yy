{
    "id": "11dc7a69-1780-4655-96df-b3c50b46b178",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blueWallSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88450a8a-07a4-4e17-aa4c-3a84dfdd23cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11dc7a69-1780-4655-96df-b3c50b46b178",
            "compositeImage": {
                "id": "998055fe-ee7d-4918-918c-8e725e76886a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88450a8a-07a4-4e17-aa4c-3a84dfdd23cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70c83929-6244-49db-9606-cf0a55f2df27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88450a8a-07a4-4e17-aa4c-3a84dfdd23cb",
                    "LayerId": "30bda5bd-26a0-431c-91fb-527b5c70c563"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "30bda5bd-26a0-431c-91fb-527b5c70c563",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11dc7a69-1780-4655-96df-b3c50b46b178",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
{
    "id": "e87f1c60-5ca2-4350-8839-fe051d1e9442",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_aKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 57,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1945f16c-0f8d-4958-a73a-de32572ec2b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e87f1c60-5ca2-4350-8839-fe051d1e9442",
            "compositeImage": {
                "id": "cac293e1-301b-452c-9ce4-c16fdd1510b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1945f16c-0f8d-4958-a73a-de32572ec2b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78882f81-c41a-47a4-9151-f4c1c0b6c804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1945f16c-0f8d-4958-a73a-de32572ec2b5",
                    "LayerId": "8a154d8d-603f-4fcf-9d51-29df91931666"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "8a154d8d-603f-4fcf-9d51-29df91931666",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e87f1c60-5ca2-4350-8839-fe051d1e9442",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 58,
    "xorig": 29,
    "yorig": 31
}
{
    "id": "57cce91d-1064-4833-bec1-75282a361b38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_checkpointStar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2d9cdbd-1c8b-4e7d-bae4-44e3ee9126e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cce91d-1064-4833-bec1-75282a361b38",
            "compositeImage": {
                "id": "b4de6f93-651f-4122-a177-5fe36bbc2957",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2d9cdbd-1c8b-4e7d-bae4-44e3ee9126e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07a6100f-2b80-4a7f-8a03-132a999806ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2d9cdbd-1c8b-4e7d-bae4-44e3ee9126e7",
                    "LayerId": "6fcd8123-73b8-494b-aaf1-fb9c8f387954"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6fcd8123-73b8-494b-aaf1-fb9c8f387954",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57cce91d-1064-4833-bec1-75282a361b38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
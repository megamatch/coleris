{
    "id": "d5e8ed89-01b9-4cab-ac39-b776ea883aca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_checkpointBlue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1f22e62-04b0-4a8c-bc18-62b2061255e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5e8ed89-01b9-4cab-ac39-b776ea883aca",
            "compositeImage": {
                "id": "55085a19-5138-46ec-b23b-da32e443b514",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1f22e62-04b0-4a8c-bc18-62b2061255e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efd60ef8-e94e-4d85-93ff-bf3b87c3474f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1f22e62-04b0-4a8c-bc18-62b2061255e7",
                    "LayerId": "c803709f-3feb-42f1-813e-25fe6fdb84b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c803709f-3feb-42f1-813e-25fe6fdb84b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5e8ed89-01b9-4cab-ac39-b776ea883aca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
{
    "id": "f16f26c5-a859-4179-92c5-b9a3d14ca1b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_colorWheelTest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99086934-1283-4f64-b3ad-bacb7674f62c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f16f26c5-a859-4179-92c5-b9a3d14ca1b6",
            "compositeImage": {
                "id": "2527facd-60e4-4808-8c57-fbf423652d75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99086934-1283-4f64-b3ad-bacb7674f62c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c65fcd0a-fc87-4bd5-abbf-5b1aa2d58c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99086934-1283-4f64-b3ad-bacb7674f62c",
                    "LayerId": "dcf44184-867a-4a46-923e-af6aeba90b0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "dcf44184-867a-4a46-923e-af6aeba90b0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f16f26c5-a859-4179-92c5-b9a3d14ca1b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
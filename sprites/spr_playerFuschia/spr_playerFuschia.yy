{
    "id": "3ebfefba-9ff7-42a0-92d6-8f9779424cca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerFuschia",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3baa10a7-2b8f-4dc3-af47-04b92a7ac1c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ebfefba-9ff7-42a0-92d6-8f9779424cca",
            "compositeImage": {
                "id": "3bf9bbb2-962f-41bb-885e-be31cc873a0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3baa10a7-2b8f-4dc3-af47-04b92a7ac1c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20880f1d-f0ca-4d3e-9618-2246896bd8b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3baa10a7-2b8f-4dc3-af47-04b92a7ac1c9",
                    "LayerId": "7f427d24-d1d0-4267-af58-8684526e14d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7f427d24-d1d0-4267-af58-8684526e14d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ebfefba-9ff7-42a0-92d6-8f9779424cca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
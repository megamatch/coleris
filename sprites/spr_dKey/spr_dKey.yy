{
    "id": "567dd2ee-34c5-493f-b0e3-2dbe2f8baeec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 57,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "648688f0-6a69-4bac-91dd-5cd49d8536a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "567dd2ee-34c5-493f-b0e3-2dbe2f8baeec",
            "compositeImage": {
                "id": "14406f89-4e12-429b-80f8-9ce96710a7da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "648688f0-6a69-4bac-91dd-5cd49d8536a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3be6ee6f-bd4a-470a-8488-37a351976531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "648688f0-6a69-4bac-91dd-5cd49d8536a9",
                    "LayerId": "02706ac5-8f6c-4ba8-91af-b9d43ad7b00b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "02706ac5-8f6c-4ba8-91af-b9d43ad7b00b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "567dd2ee-34c5-493f-b0e3-2dbe2f8baeec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 58,
    "xorig": 29,
    "yorig": 31
}
{
    "id": "dacf4810-543a-475a-b523-df7fb7263724",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_space",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 203,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d4e1921-7006-4235-bccb-6c2e46d6488b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dacf4810-543a-475a-b523-df7fb7263724",
            "compositeImage": {
                "id": "25326660-bf3c-4ac4-bcbd-79ea07e287c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d4e1921-7006-4235-bccb-6c2e46d6488b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b206742-a0dd-4a5a-b27c-d406322cf89e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d4e1921-7006-4235-bccb-6c2e46d6488b",
                    "LayerId": "f30e7e12-1c3d-480b-80b8-fe3018285854"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "f30e7e12-1c3d-480b-80b8-fe3018285854",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dacf4810-543a-475a-b523-df7fb7263724",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 204,
    "xorig": 102,
    "yorig": 31
}
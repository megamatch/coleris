{
    "id": "21321d64-6a02-4009-aa10-742b82b08948",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_greyWallTriLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "883086d2-e5af-4919-8339-1d7c8f85b0c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21321d64-6a02-4009-aa10-742b82b08948",
            "compositeImage": {
                "id": "8e1f0c38-01a6-477d-99ae-699c59b1792e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "883086d2-e5af-4919-8339-1d7c8f85b0c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad54c97f-b40a-476a-b2fe-22531f8a12af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "883086d2-e5af-4919-8339-1d7c8f85b0c7",
                    "LayerId": "56f71694-ec90-4e90-bcc8-589a0f0070ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "56f71694-ec90-4e90-bcc8-589a0f0070ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21321d64-6a02-4009-aa10-742b82b08948",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
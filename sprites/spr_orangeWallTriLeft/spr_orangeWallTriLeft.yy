{
    "id": "fbae3a3e-5610-4f60-ae07-63bf243f62f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_orangeWallTriLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fd26908-b0bd-4728-a201-204e8b192fdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbae3a3e-5610-4f60-ae07-63bf243f62f1",
            "compositeImage": {
                "id": "e1774233-ddd7-4c02-a398-4fb24482afd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd26908-b0bd-4728-a201-204e8b192fdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca8f5ad4-fade-44e2-bf05-802b21b16a8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd26908-b0bd-4728-a201-204e8b192fdd",
                    "LayerId": "8b102779-0527-4d1b-b9c0-927126395eb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8b102779-0527-4d1b-b9c0-927126395eb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbae3a3e-5610-4f60-ae07-63bf243f62f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
{
    "id": "b3e3ac4f-f649-4e9d-9423-2f19bf5f9c65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_starGoal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 12,
    "bbox_right": 116,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91520e4c-756d-412e-b7cd-101f6ac34701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3e3ac4f-f649-4e9d-9423-2f19bf5f9c65",
            "compositeImage": {
                "id": "0b42b5ca-c65e-455c-9d5e-1083b7a21d25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91520e4c-756d-412e-b7cd-101f6ac34701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c39345e4-f216-44e0-a64d-df39186f13ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91520e4c-756d-412e-b7cd-101f6ac34701",
                    "LayerId": "fdace176-0e67-437d-8adf-ab34a1816f60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "fdace176-0e67-437d-8adf-ab34a1816f60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3e3ac4f-f649-4e9d-9423-2f19bf5f9c65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
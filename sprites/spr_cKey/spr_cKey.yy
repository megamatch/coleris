{
    "id": "ba7611d4-32f2-4943-8de0-066e641ce635",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 57,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "756da370-b373-4c67-be40-61fb81213c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba7611d4-32f2-4943-8de0-066e641ce635",
            "compositeImage": {
                "id": "8ced8eef-d269-4aac-994e-56a2aaa010aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "756da370-b373-4c67-be40-61fb81213c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1239a09a-e430-4191-9a0e-1fa31a7fc3e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "756da370-b373-4c67-be40-61fb81213c5b",
                    "LayerId": "445277f4-b7a5-4897-b9fb-b63ed30d844b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "445277f4-b7a5-4897-b9fb-b63ed30d844b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba7611d4-32f2-4943-8de0-066e641ce635",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 58,
    "xorig": 29,
    "yorig": 31
}
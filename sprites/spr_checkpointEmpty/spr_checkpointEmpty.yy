{
    "id": "bdefaaa9-9b90-439b-8d3e-5974499af10b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_checkpointEmpty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48fe7e9c-29cc-4613-8876-665449d21651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdefaaa9-9b90-439b-8d3e-5974499af10b",
            "compositeImage": {
                "id": "9eda7df2-f713-4e37-b329-5b7a095ba1ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48fe7e9c-29cc-4613-8876-665449d21651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18615945-effe-4e82-9ca1-2f02c82d38c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48fe7e9c-29cc-4613-8876-665449d21651",
                    "LayerId": "7aa987fe-ebea-4f09-940b-0edd1e3414b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7aa987fe-ebea-4f09-940b-0edd1e3414b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdefaaa9-9b90-439b-8d3e-5974499af10b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
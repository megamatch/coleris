{
    "id": "bd17f523-da1a-47ab-9c5b-38b28562e941",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_orangeWallSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51b4987f-b460-4cfa-9dad-ae4e15ed28a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd17f523-da1a-47ab-9c5b-38b28562e941",
            "compositeImage": {
                "id": "1318d158-34da-40bc-94f8-b17eaf235d00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51b4987f-b460-4cfa-9dad-ae4e15ed28a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e632e31-be0e-4141-b380-872c29559488",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51b4987f-b460-4cfa-9dad-ae4e15ed28a0",
                    "LayerId": "21e98313-5d06-41c6-a107-958261c272b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "21e98313-5d06-41c6-a107-958261c272b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd17f523-da1a-47ab-9c5b-38b28562e941",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
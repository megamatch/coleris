{
    "id": "76576b9d-6834-4b27-a7fe-1df5b4fd5f3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerOrange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "076f962e-9f02-4364-8a0b-7e52ad0c7b7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76576b9d-6834-4b27-a7fe-1df5b4fd5f3e",
            "compositeImage": {
                "id": "85ce0898-657d-4d6d-ae03-e6b6830010c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "076f962e-9f02-4364-8a0b-7e52ad0c7b7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd20f95-b383-4105-91ba-85a7eeb4b92d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "076f962e-9f02-4364-8a0b-7e52ad0c7b7f",
                    "LayerId": "2996fc97-717b-4c19-9c90-2091e173db96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2996fc97-717b-4c19-9c90-2091e173db96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76576b9d-6834-4b27-a7fe-1df5b4fd5f3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
{
    "id": "9812110b-da0d-46a1-84fc-9c1fe4bd92d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_checkpointOrange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbbf030e-50d9-47ff-b2b5-79d07b068243",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9812110b-da0d-46a1-84fc-9c1fe4bd92d5",
            "compositeImage": {
                "id": "2af58830-6708-4f50-9f48-e8278902299d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbbf030e-50d9-47ff-b2b5-79d07b068243",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1de03b57-9cf8-4c92-a171-188e63dfcf49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbbf030e-50d9-47ff-b2b5-79d07b068243",
                    "LayerId": "dc615a62-2d67-4017-b2d8-7a38541efbc1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dc615a62-2d67-4017-b2d8-7a38541efbc1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9812110b-da0d-46a1-84fc-9c1fe4bd92d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
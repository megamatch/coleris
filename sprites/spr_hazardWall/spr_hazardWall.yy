{
    "id": "189c9569-9674-4ffd-a6ce-088a42024e5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hazardWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7e28abe-f263-4218-92d2-3b639e31b373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189c9569-9674-4ffd-a6ce-088a42024e5e",
            "compositeImage": {
                "id": "b772e5ab-0578-4eb0-91b0-6e594523c37f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7e28abe-f263-4218-92d2-3b639e31b373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86369db8-98d3-45c6-b64a-6e98acacf377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7e28abe-f263-4218-92d2-3b639e31b373",
                    "LayerId": "e46d11cb-da68-4e00-b67d-e8ace3b6a9fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e46d11cb-da68-4e00-b67d-e8ace3b6a9fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "189c9569-9674-4ffd-a6ce-088a42024e5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 64
}
{
    "id": "dcc4f1c8-da59-4c07-ab39-f70a98347e6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_greyWallSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62cea41d-9c6c-4963-81a0-15c7c38194c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc4f1c8-da59-4c07-ab39-f70a98347e6a",
            "compositeImage": {
                "id": "d8aad7bc-d019-4113-b594-3b4c2b812903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62cea41d-9c6c-4963-81a0-15c7c38194c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7334de-a86d-4e47-9d5d-2bab77074247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62cea41d-9c6c-4963-81a0-15c7c38194c1",
                    "LayerId": "67f56fe8-6ba3-44fb-aa62-0f02aab8bde7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "67f56fe8-6ba3-44fb-aa62-0f02aab8bde7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcc4f1c8-da59-4c07-ab39-f70a98347e6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
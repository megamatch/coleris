{
    "id": "a994bb10-6ead-4ce9-8c34-fce93f9543d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fuschiaWallSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ce7d414-978d-4d3b-a43a-c0861d0cfbed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a994bb10-6ead-4ce9-8c34-fce93f9543d0",
            "compositeImage": {
                "id": "470ae3e6-b115-447c-9d72-05d43d2ef62d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce7d414-978d-4d3b-a43a-c0861d0cfbed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5d09978-0c95-4867-847a-df46782c74c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce7d414-978d-4d3b-a43a-c0861d0cfbed",
                    "LayerId": "835f9125-5b74-4070-89d1-1ea4739567dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "835f9125-5b74-4070-89d1-1ea4739567dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a994bb10-6ead-4ce9-8c34-fce93f9543d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
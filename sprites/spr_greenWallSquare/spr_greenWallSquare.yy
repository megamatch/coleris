{
    "id": "57344181-8f57-42ed-a472-ae21c40fdf07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_greenWallSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d954d54f-c854-4ef5-9e17-2bf500eb62f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57344181-8f57-42ed-a472-ae21c40fdf07",
            "compositeImage": {
                "id": "20af23d5-c339-4d98-9210-0cf1227ed847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d954d54f-c854-4ef5-9e17-2bf500eb62f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cef4d513-4a72-4b29-8f4a-46e3891bbe86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d954d54f-c854-4ef5-9e17-2bf500eb62f3",
                    "LayerId": "6e4304de-7e7f-4fe7-a35b-bd91df23de3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6e4304de-7e7f-4fe7-a35b-bd91df23de3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57344181-8f57-42ed-a472-ae21c40fdf07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
{
    "id": "0e704c05-1f91-4a1d-9323-f390b3189d6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blueWallTriLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fde9e525-c8fe-49b5-8593-37995e9d7019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e704c05-1f91-4a1d-9323-f390b3189d6e",
            "compositeImage": {
                "id": "06931795-5254-4177-939d-c56829c6c563",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde9e525-c8fe-49b5-8593-37995e9d7019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4322bb84-7569-4d35-b42b-21d37bd01703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde9e525-c8fe-49b5-8593-37995e9d7019",
                    "LayerId": "61e432b6-b5a2-4c28-9fd3-fd105db5764a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "61e432b6-b5a2-4c28-9fd3-fd105db5764a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e704c05-1f91-4a1d-9323-f390b3189d6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
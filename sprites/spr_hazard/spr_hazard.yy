{
    "id": "16c6b90e-4088-4fa8-915e-3f2dd9569cf2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hazard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 1,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7b3b93f-e7fc-48f1-93e2-dbf9988d7b27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16c6b90e-4088-4fa8-915e-3f2dd9569cf2",
            "compositeImage": {
                "id": "47045679-561d-4935-bb27-24a365dd1706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7b3b93f-e7fc-48f1-93e2-dbf9988d7b27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7454ff67-9218-4b05-9813-b1a168100489",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7b3b93f-e7fc-48f1-93e2-dbf9988d7b27",
                    "LayerId": "a4bd21d9-e7a2-449d-8539-439571dffa0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a4bd21d9-e7a2-449d-8539-439571dffa0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16c6b90e-4088-4fa8-915e-3f2dd9569cf2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 64
}
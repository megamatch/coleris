{
    "id": "3e5b5609-ddd8-4437-9ea8-42ccb62a6c35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hazardTri",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebdc2a4d-bee4-4a82-8b84-4f7523434b5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e5b5609-ddd8-4437-9ea8-42ccb62a6c35",
            "compositeImage": {
                "id": "9dd96108-ea3f-46ee-ba25-7103ce6b4875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebdc2a4d-bee4-4a82-8b84-4f7523434b5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "905cf241-fc24-409e-ae12-e5c9cd0b949c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebdc2a4d-bee4-4a82-8b84-4f7523434b5a",
                    "LayerId": "97749782-907d-4f7d-8671-79c76dbf5513"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "97749782-907d-4f7d-8671-79c76dbf5513",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e5b5609-ddd8-4437-9ea8-42ccb62a6c35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
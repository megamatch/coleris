{
    "id": "e39e4839-f2dc-4c8d-8aa0-c6bcd92598f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerBlue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cb615dc-26a3-4065-a3d1-47c4f150e98a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e39e4839-f2dc-4c8d-8aa0-c6bcd92598f0",
            "compositeImage": {
                "id": "c137f2c4-51b8-42e5-9593-9ce2e66894f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cb615dc-26a3-4065-a3d1-47c4f150e98a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2bfb87e-8f76-46ed-b98b-92e69c16120f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb615dc-26a3-4065-a3d1-47c4f150e98a",
                    "LayerId": "a4b9e6b4-4515-40e2-92ac-54049b3154a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a4b9e6b4-4515-40e2-92ac-54049b3154a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e39e4839-f2dc-4c8d-8aa0-c6bcd92598f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
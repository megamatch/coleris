{
    "id": "822024bc-b4af-408a-9405-b2ce7943dd01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_checkpointGreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68e8ecae-06ef-42c5-b8e0-34c70a7d00a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "822024bc-b4af-408a-9405-b2ce7943dd01",
            "compositeImage": {
                "id": "7f754b84-2453-4ca1-b583-762fd24b9741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68e8ecae-06ef-42c5-b8e0-34c70a7d00a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "402255c6-7b7e-4809-9137-ed63dab17b7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68e8ecae-06ef-42c5-b8e0-34c70a7d00a0",
                    "LayerId": "464de1ab-85e3-4cda-a3f6-f6dd9ba61719"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "464de1ab-85e3-4cda-a3f6-f6dd9ba61719",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "822024bc-b4af-408a-9405-b2ce7943dd01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
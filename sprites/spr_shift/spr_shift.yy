{
    "id": "f5de60e4-1ed2-4e64-89da-ebc919e15e11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shift",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 128,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95490bb8-3037-43ee-a4f9-12e95416f7cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5de60e4-1ed2-4e64-89da-ebc919e15e11",
            "compositeImage": {
                "id": "bb900823-5341-4337-ade5-de6e7d310b1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95490bb8-3037-43ee-a4f9-12e95416f7cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ca90386-a797-40a4-acf4-768059ad19a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95490bb8-3037-43ee-a4f9-12e95416f7cb",
                    "LayerId": "d0864ae4-9543-497a-a999-8d8ff25e8705"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "d0864ae4-9543-497a-a999-8d8ff25e8705",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5de60e4-1ed2-4e64-89da-ebc919e15e11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 129,
    "xorig": 64,
    "yorig": 31
}
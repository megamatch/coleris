{
    "id": "93203c72-35d6-4793-a0c7-6d3d054b2258",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_testingOverlay",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b6dd5f4b-de8f-4b40-a074-2418639d9348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 94
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "af863559-6259-42d1-ac0a-1d809c9c96bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 45,
                "y": 94
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "07b6bd2f-5b19-44f0-aa4d-b27c103221b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 229,
                "y": 71
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "03251167-5c03-4e73-b414-c0d87fd564f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 195,
                "y": 25
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "486ae8fb-6999-4af5-b95a-9f3b6efd1abb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d3704486-9610-4e89-a2a6-bab9a3398134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5c118b44-778d-4446-889b-bb0ab95cba52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f7122402-b617-4101-a947-ffde066db702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 35,
                "y": 94
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2afe3c21-a8a8-490a-9f96-ed6cbf44283a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8679abad-5074-4ca2-9ec8-2cec9c4e7696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 94
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6851b17b-780b-4939-bf5b-d9489bc6aa60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 148,
                "y": 71
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "96e17836-add2-41e0-8562-5a37d25cb7fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 134,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8db25f38-d960-4644-9f79-50d7d060728b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 40,
                "y": 94
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5c84ed88-1cc2-4c09-89e1-459079e111e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 157,
                "y": 71
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c920b1e7-217a-46a4-8d5f-cd08af7fae1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 50,
                "y": 94
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c97b1226-eb24-4830-a7f7-4db2d322cb37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 173,
                "y": 71
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9ad4122e-6cfa-4600-93e8-03751690709b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a3648e4f-1431-452a-8afe-ede82613d673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 213,
                "y": 71
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "870e9c89-90cb-409a-83e3-3c9babb6362d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 122,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "42c47e25-5919-4061-8de3-cb4d4463ed7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 146,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "860934cf-59b6-4c4e-8a94-7732b64bbf54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 71
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a44e93b7-c081-452d-9ff1-43f9f109b887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 242,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fb4a73d2-31e9-4225-95cf-2b1dac8aa20b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 230,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bb8c7a74-1cb1-4757-8b0d-9dd6add9725f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 218,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "dd24797d-94d4-488a-9496-3e76dacaead0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 206,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5f1db110-d791-465a-ae08-822d7f1d08bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 194,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "79277ddd-34ef-4f58-aaeb-ced9de07afd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 60,
                "y": 94
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "edd0db5c-b135-4820-87e3-39e8b35f0e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 65,
                "y": 94
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c980273f-d643-4e74-97e3-f5cb131aedca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 48
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c2087677-15c1-432b-b14a-6ab0f8a40f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 208,
                "y": 25
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f9905c38-85ce-4c4d-869d-fa341e3cc975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 158,
                "y": 48
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a97a5deb-1cc4-4e12-ac5a-79afa39bef8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 170,
                "y": 48
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "72d8e80f-9b60-40c5-ac5a-56f5508c5b26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d68a9e58-4610-47c9-ab8a-f7da72b377fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ea63dca2-c23c-4aa1-9a6a-0edd96622458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 156,
                "y": 25
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "49aff080-8bb1-411e-b730-61730a8e6f0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b703a3d7-58b7-4c12-8dce-9931ecbc9bac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 17,
                "y": 25
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "dc78bc10-013f-40a2-ace5-8e8be5906629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 143,
                "y": 25
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4c2f53e2-9e05-440d-a17f-0e1eacd471fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "82fc3636-3e9f-4c26-9009-baa25871e395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fbabeb76-ed3a-4dd4-b180-f7fc4e7184ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 31,
                "y": 25
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "130c34cb-a09e-4203-996a-68cb48843866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 55,
                "y": 94
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "daea0d1e-0e0c-4c14-9dbb-b277f66207e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 82,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b7b9322a-6ef2-458a-97dc-1e77c43fe45e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 45,
                "y": 25
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e95d5000-a2b7-4cc4-8773-11a153f0c3e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 104,
                "y": 71
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d92f4174-ecae-48a5-bc5b-ef3b76e845ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3914ce6a-1b13-45f2-aa3d-a1fa12b26fc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 59,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6be2907f-8a18-4013-9739-912c8e0414c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "77f2e0c5-6b38-45d2-8778-6eca19459a62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 169,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "54da90c3-8dbb-4af5-aa4c-cc9bdf0773fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "6313cc09-ab0b-4f1a-9b33-033b5766eaf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "47e14973-332f-422d-8a24-331b9981e2b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 129,
                "y": 25
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f28fbd6e-ee11-445b-8a45-d9e8325c87cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 87,
                "y": 25
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5183f7d4-c345-4dfc-9058-b182033c26eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 101,
                "y": 25
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e6377fcc-e7c6-4195-90d7-1d142295ef51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "63cf9f0e-0a25-434f-b662-f85928c60c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7924ed96-8045-4ddc-a9cb-a5295eeb3484",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8a44d28a-7003-4be3-8806-841b405f978f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3a22747c-0394-41cb-b111-d2a812e89fca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 115,
                "y": 25
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d681c814-4f1c-4bec-ab7f-03fffd7bf2eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 29,
                "y": 94
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ac917e25-4ef6-496c-8676-77b87315a22b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 165,
                "y": 71
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "923707f1-06fc-4cff-9363-4aee326e7a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 237,
                "y": 71
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ed17e059-de17-4e45-ab29-4100b4f2d8d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 137,
                "y": 71
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f1888f36-a7ec-436e-810a-d66ee5654063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 73,
                "y": 25
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "233a306a-1960-4673-a5dc-dc44abbfe23f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 244,
                "y": 71
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "75ba36c5-6535-435c-87e3-c7091845dae1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 48
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ad1d54d6-9038-4329-9ea5-adc084c97aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 93,
                "y": 71
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d67cbf22-58a6-4494-973f-6a6410802cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 48
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "dcfb4577-af6b-4e41-baaa-0e0618911b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 48
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a37ee0f8-c9ef-4374-b49d-28f50477e071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ff323db4-4faf-4012-8598-b0fa8ec1b106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 197,
                "y": 71
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9ecad7d0-6dd0-489c-b445-3ccb78dc546f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 232,
                "y": 25
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a2853b29-5ad4-4021-9d59-57187d78dab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 71,
                "y": 71
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "63cd0677-86a1-4fcb-8751-f12ef7b9f34c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 75,
                "y": 94
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a128c299-dace-461d-a8af-481dd471c0d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 23,
                "y": 94
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6ae52dda-531c-4847-85ac-9e59f72a8166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 49,
                "y": 71
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e87c4d3b-0e22-4b82-a20b-4466641f03a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 79,
                "y": 94
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dbcbaa31-3d1a-4c6d-a05e-683e867ff2cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a1bf2427-afda-4355-adc4-c75fe34ddd12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 115,
                "y": 71
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3edb065a-e597-4b2b-b7aa-ec170adf5b02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 220,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e4cd104e-654d-4749-a4e7-4497bdfe0dad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 60,
                "y": 71
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e63d086b-399f-483f-8463-c3de08f27c6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 48
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0993c70c-9dc3-4055-96f8-07017352173c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 181,
                "y": 71
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b5e7fb3d-55df-43b8-9e08-b03d9161f61d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 126,
                "y": 71
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "fc6773ec-1094-40f6-92d8-af0c5302ebdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 221,
                "y": 71
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "91efed69-2438-4013-9903-2102676be2f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 38,
                "y": 71
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2c0447f2-7b1b-4f17-8767-1b6dc3a0adec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 48
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e70bbf9b-cf10-4ed8-b3ee-dab521dec774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5acb70c5-0f32-4074-ad04-dfbad6759e3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 48
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c60c536a-bb8b-4eea-a3ae-0400d3587ca8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 71
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "59e57f40-70ec-4b49-9890-47e15c0ccfbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 182,
                "y": 48
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "32af3d98-9fe1-4fa8-92c8-6fe0d5e0abc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 189,
                "y": 71
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "70a4bf88-7431-46cd-985d-5c846d543cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 70,
                "y": 94
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "284322e5-45a0-4f3a-89e1-a5d299b22cf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 205,
                "y": 71
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c92f0e57-9604-4c18-8d63-6558089eee7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 182,
                "y": 25
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "5ed6991e-b4ed-48f1-8fbc-14de40481c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "045cc189-9883-4ac4-aa94-3a1237710a36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "aa3042ec-5659-4636-9835-c0342b29f632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "b527e624-cde0-4c91-84be-c1b2ed7ab7b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "6dca8a34-f0bd-49b5-bf48-d55b6404d9f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "c8a34453-114b-4e51-b1c2-4745a1b3be0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "692fe373-ce4b-4a3a-923f-fd5e7b4ace5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "4ed8bde2-cd94-4f60-bb7b-e35412255174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "f12ccd3a-dd73-4fc9-88dc-e07cde1daf42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "b8dac5d6-049f-4972-a8a6-1394854c611e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "75bcae4e-7a8f-4f4a-a4ac-0f602b298042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "5ae921aa-6c87-42f0-ace9-dd1f0368b037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "f83e953b-0e6e-4f73-8d2d-ddaa2f17ad73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "b2352c7e-bb13-4d22-9735-a9939d15e7ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "df9c94b3-6a02-4228-b8bb-ff5728f6f6b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "52929274-71df-4d95-b459-06e357d66d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "2f398f74-2466-4454-8ee1-b225234e7f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "52718681-d9b8-458f-b955-dce691c04a7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "3a5d3794-51e8-4eff-9b8d-a078b540db93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "774810b8-89ff-43b5-957d-82232c594bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "8d58f090-73a3-4a24-a2da-4eb2fc76a5f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "f03ebe1c-6648-410f-8fc7-330bfebf2c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "2a7648c6-2982-46c6-ab88-81455d49c7dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "ca5747b2-6cc6-4c50-81f6-e8211114d45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "ccad43ca-c004-4b33-afec-26bf28aea420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "21d54b73-cc13-4624-8703-aafec60d9d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "9b7c9e16-757c-4cf5-878f-5c3f4cdedaf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "5b93fce8-6818-4d36-b507-b3c0358ff616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "ccfdd571-e840-4950-90af-42341cee0d02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "150e48d7-39ba-42a8-86d6-f4c9ddbd0774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "b38fa307-2ee9-455e-b0f9-939fc727386c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "5a776b1d-5b2c-498c-9ea1-727fc754fed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "16444a67-4625-4014-aae0-937f6f629469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "08d2c9ba-abf3-47aa-aacc-3c0a7be2f968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "0d787069-0c21-42ab-98de-85028d5e790d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "adf948ff-4f02-4152-adc1-714e9d3dc245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "205b53c9-7da6-4be1-8b86-d66ae596d493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "9731cc62-a1fc-4c5d-904d-d9749391b3c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "715a182a-7e13-4b20-b08f-a3bb23c7fde0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "8539f59f-8430-41e2-ad5f-41e9c237130a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "a60512eb-0c75-4511-982a-4c9f0bc6527d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "29b435cd-927e-4551-8a55-e9f921b05fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "008b04e4-f26e-43b4-bdca-99471c00a0c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "20dc9185-f112-44f0-a742-61f342fe0c47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "06bc1274-c523-4e02-a3f7-898452392659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "ff8fd594-95dd-4146-b58b-0479e475746a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "54983f09-9531-4e90-96a0-7cb8ff3dfdeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "9ae99739-bd2f-4b87-bce7-e6c13974e79f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "b2479baa-5ca9-4b70-8bf6-2e553f758291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "608dfc24-b752-4b94-b692-1bb62c63523d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "7f0b61e5-ff61-4351-8d2a-5953417fdbf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "be778662-ed3f-4b48-a1b4-bed1188730f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "2c1ea172-8b05-4a71-9fd7-fd2005f17301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "a51c42e2-030a-4be7-a3a5-f42ef86248cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "f4399f8b-d022-41b5-8491-bc9e74fce37e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "7c10165b-3bbf-4294-a649-01da1f42b758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "d558186f-e1ab-47ee-82bf-d8a9de028269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "4b151598-3b63-4034-a4b6-40e05688b198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "ad6b8c89-ca46-438c-a44d-7b87c7509156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "d0006b81-e904-4aa5-975a-499102cf8316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "1745530c-a586-4d2e-bb88-db7e9ebefc37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "3fbb0462-fdf4-467e-8032-33b7788427e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "7d0bf65b-b91a-4b84-873d-274ace52b5f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "ddb9f294-0de1-4dba-a15d-0b9595b50513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "db44c476-42ae-4b05-a0e3-bc787535d9aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "abc29302-654f-4555-80bc-908ad5849943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "b9ae14ee-d3be-453f-9c37-c88b95cf2621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "26fb0843-f160-4492-beaf-0b65a3f7beb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "8e9a5842-e1d8-4270-80a5-3a0de3ccc3d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "33901fa1-4293-40fb-a0e0-1b672a0f1d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "abdb3763-b320-442a-9d3f-a99f694d66c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "18b204b7-58ef-465b-aa63-2228b6aa82a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "719eece7-ad97-47fb-9584-22a0eafd4461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "bef8f90a-c00e-4205-8b36-5bdfec6243b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "5897a7a1-8154-4d08-810d-8f97767f5fc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "8082d485-a84a-4231-9b27-c5f90ca6abd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "46009a03-e8d0-4011-8d71-e88792363ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "7265f3de-7ce1-4fb5-9dd2-8aaaa8f9589b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "96de4e9b-a373-44fd-9663-4367a2844557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "e89e9b3c-ae9d-43fc-b55d-c4e0debb6dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "acd6688f-bdf0-4e4a-8fbb-c3e4862ce98c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "3049bc39-8996-4a79-87c2-f4326448a092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "e3e8df62-4a7f-4630-8a63-e97328d59e49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "53c39918-967c-4a85-be2f-83167e346c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "8feaf332-af06-4b46-a7fe-8c1bbf6e696b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "79fabccd-bd6d-47a5-82d1-f76b9078f81a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "d1e68c51-5a56-4afb-b791-8d6da978a172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "f26fc717-f2e1-4b57-aae1-5e127c59d637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
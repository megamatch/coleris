{
    "id": "70d3efb1-cc4a-4f5f-8d50-a968959a101e",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_Menu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial Rounded MT Bold",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3b16d171-5eba-4adc-b780-48facd9cc28d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 74,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 979,
                "y": 154
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e314bbc5-19f3-4e17-9cbb-6aeaad062774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 74,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 49,
                "y": 230
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "da7c2ae2-3556-4a2c-8f17-1f2c7b88b267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 74,
                "offset": 3,
                "shift": 31,
                "w": 25,
                "x": 703,
                "y": 154
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "366f9230-0424-4e2f-a91d-2bc7862031f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 74,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 312,
                "y": 78
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8072fb74-b483-46fd-a663-46b638b0de16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 74,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 709,
                "y": 78
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "206baa80-fabc-4f94-a49a-3058f5097da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 74,
                "offset": 1,
                "shift": 55,
                "w": 53,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "70d10d24-c7e9-4a51-8bc3-72110bc2ce1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 74,
                "offset": 3,
                "shift": 49,
                "w": 44,
                "x": 523,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3298c6ef-d4ce-4d3f-845c-e03f7309c475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 74,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 75,
                "y": 230
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "db330fc1-2c5e-4ffd-b669-59501d7a9479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 74,
                "offset": 4,
                "shift": 23,
                "w": 16,
                "x": 997,
                "y": 154
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "33ab2bea-65d8-45ad-9038-68d36d75ae7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 74,
                "offset": 3,
                "shift": 23,
                "w": 16,
                "x": 2,
                "y": 230
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d88b3118-a0dd-453a-a334-175b17d1eeba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 74,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 647,
                "y": 154
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "548d2136-3452-4ace-a6fd-0c2c0e4564b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 922,
                "y": 78
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7cb3a4c7-4a9b-4cd1-b3da-50a9e2533edd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 74,
                "offset": 5,
                "shift": 20,
                "w": 11,
                "x": 36,
                "y": 230
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "772b1381-879c-4460-889f-d3fee5fb8852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 74,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 856,
                "y": 154
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c634d2ca-2f0e-4845-a1f5-9efbe038a86f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 74,
                "offset": 5,
                "shift": 20,
                "w": 10,
                "x": 88,
                "y": 230
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d7dae1c8-dd9a-4d79-8b1b-5156ab4eb20d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 74,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 919,
                "y": 154
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f644bd41-bf53-4a0d-b6a6-5c1305990ffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 317,
                "y": 154
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fe16fb90-6515-48be-a775-b50f7df189b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 74,
                "offset": 4,
                "shift": 38,
                "w": 23,
                "x": 782,
                "y": 154
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d5223e4b-e30c-4325-8ee9-aebdf8da62e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 2,
                "y": 154
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f3383e76-b880-49d7-881f-93effcf613c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 37,
                "y": 154
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d893d5bc-7950-440d-a8d8-e2a4559229ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 74,
                "offset": 0,
                "shift": 38,
                "w": 37,
                "x": 83,
                "y": 78
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3a8b488c-c94b-4af6-a03a-85ae73c799c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 72,
                "y": 154
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0acc8e23-ab9d-4f6f-8b4a-9710e84a0129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 74,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 745,
                "y": 78
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5032318a-d05a-4789-85f9-5e3851b4019b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 74,
                "offset": 4,
                "shift": 38,
                "w": 33,
                "x": 177,
                "y": 154
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4607c03f-96af-407a-ab65-9e351739b35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 74,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 781,
                "y": 78
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b9a7d53e-6fc9-4e35-baa7-e8ab20feaa0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 74,
                "offset": 2,
                "shift": 38,
                "w": 33,
                "x": 142,
                "y": 154
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7f80bcc3-9cb3-4303-a314-32803ee46d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 74,
                "offset": 5,
                "shift": 20,
                "w": 10,
                "x": 112,
                "y": 230
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2eff6ee8-7632-4ffb-8cf6-94aca959ff15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 74,
                "offset": 4,
                "shift": 20,
                "w": 11,
                "x": 62,
                "y": 230
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6a8b38cf-56e5-4f13-92f1-fccd8e6fbc0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 247,
                "y": 154
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6f2f047b-02a4-4804-83a8-334d5aee010c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 212,
                "y": 154
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1208e108-6f5b-4570-aec6-bfc20c26f280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 282,
                "y": 154
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "98a76b35-10d0-4673-a842-bfcf15144e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 817,
                "y": 78
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0816156f-5048-457a-bd66-2b4942df9e03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 74,
                "offset": 1,
                "shift": 63,
                "w": 62,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "33788f4d-64c5-49cb-9eb6-416627c9ace8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 74,
                "offset": 1,
                "shift": 46,
                "w": 44,
                "x": 431,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8f8c3d50-d3d8-44e4-93e3-fe84bc50c59d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 74,
                "offset": 4,
                "shift": 46,
                "w": 39,
                "x": 950,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7011c460-c283-4a04-a194-b682b62c86d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 74,
                "offset": 3,
                "shift": 47,
                "w": 42,
                "x": 613,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "12dcd258-8770-4821-8163-c131595eb28f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 74,
                "offset": 5,
                "shift": 47,
                "w": 40,
                "x": 700,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "915c40df-46cc-44b8-b604-9809c299b2b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 74,
                "offset": 5,
                "shift": 43,
                "w": 36,
                "x": 200,
                "y": 78
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e219463a-62a1-42f5-b152-010ffccb1b0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 33,
                "x": 957,
                "y": 78
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f0f19bbf-a721-4b2f-9902-a0b960110992",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 74,
                "offset": 3,
                "shift": 51,
                "w": 44,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "56ae5c3a-d0ef-484c-be3b-580c59166759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 74,
                "offset": 5,
                "shift": 49,
                "w": 39,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "486e5cbd-8386-4964-b84c-ccc32b629735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 74,
                "offset": 5,
                "shift": 20,
                "w": 10,
                "x": 100,
                "y": 230
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "38f98db9-e336-4490-9d6d-fa31cd7b606f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 74,
                "offset": 1,
                "shift": 37,
                "w": 31,
                "x": 451,
                "y": 154
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "022536a7-8902-4e5b-98f3-e0348e634ff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 74,
                "offset": 5,
                "shift": 47,
                "w": 40,
                "x": 742,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "63d47a8c-2105-48f1-8103-5ca4ebe68843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 34,
                "x": 493,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e56ce36a-2e41-4b85-8c00-26832b6e8b64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 74,
                "offset": 4,
                "shift": 53,
                "w": 45,
                "x": 384,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e08f990f-e24c-4e18-bef2-5b0a02a73ce9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 74,
                "offset": 5,
                "shift": 49,
                "w": 39,
                "x": 909,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4c27caa0-2962-4020-9dcf-6bc91757e330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 74,
                "offset": 2,
                "shift": 51,
                "w": 46,
                "x": 336,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f9279aec-31d2-4d66-add0-4a5ec9ce8977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 74,
                "offset": 5,
                "shift": 43,
                "w": 35,
                "x": 275,
                "y": 78
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c240360c-058c-4084-83c7-920589b7a65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 74,
                "offset": 2,
                "shift": 51,
                "w": 49,
                "x": 285,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1b821e1c-d7cd-401c-9100-ff64a2da028d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 74,
                "offset": 4,
                "shift": 46,
                "w": 40,
                "x": 826,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "039a9139-5d8b-4d8b-801b-0abc4b6ed2a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 74,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 122,
                "y": 78
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "caa2d5e5-51a3-46bf-924c-df1d9496f908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 74,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 784,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "da104180-7550-4a9c-b3c3-bb7b204da000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 74,
                "offset": 5,
                "shift": 49,
                "w": 39,
                "x": 868,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4f80f939-f5e3-43e3-935a-c5feb8c31df2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 74,
                "offset": 1,
                "shift": 44,
                "w": 42,
                "x": 569,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4563fea3-83ef-45bf-81f1-50e232cee3f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 74,
                "offset": 1,
                "shift": 60,
                "w": 58,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9d754a20-497f-42b0-a978-1bd2b95dc1c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 74,
                "offset": 1,
                "shift": 39,
                "w": 37,
                "x": 161,
                "y": 78
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e1133e2e-2ef9-4156-99ab-d6f2a0b01931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 74,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 43,
                "y": 78
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "92463cdc-9b20-4b97-8eb8-bec221b6adc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 74,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 657,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "acd9cfa5-4345-4113-abc1-c447b5cdc26b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 74,
                "offset": 4,
                "shift": 23,
                "w": 18,
                "x": 959,
                "y": 154
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a9ca59e1-c947-433a-ba4b-a66b5b63c52c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 74,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 899,
                "y": 154
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bfceee27-3495-4e5f-9787-23c8910f2fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 74,
                "offset": 0,
                "shift": 23,
                "w": 19,
                "x": 878,
                "y": 154
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9077c7e2-5534-4dc3-b17d-6638bf3065f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 74,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 550,
                "y": 154
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "41e1678f-18cb-4284-b173-e7a5a3126f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 74,
                "offset": -1,
                "shift": 32,
                "w": 34,
                "x": 529,
                "y": 78
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a33bfb75-f259-4e14-a76f-00d0df9451b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 74,
                "offset": 2,
                "shift": 21,
                "w": 14,
                "x": 20,
                "y": 230
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "55d691c1-038b-4fd9-a0da-e2454d69b4a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 74,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 601,
                "y": 78
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8efeffb4-5db2-49b4-87c5-ccc22e40cf48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 74,
                "offset": 4,
                "shift": 40,
                "w": 34,
                "x": 385,
                "y": 78
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9aa595b1-175a-4751-a603-5945a1b6d401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 74,
                "offset": 2,
                "shift": 38,
                "w": 33,
                "x": 107,
                "y": 154
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1509c1ae-114f-4b37-9dba-972b957512e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 74,
                "offset": 2,
                "shift": 40,
                "w": 34,
                "x": 349,
                "y": 78
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9ec00357-f4a0-4627-b915-9336c95d546c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 74,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 637,
                "y": 78
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "59d32cac-647e-408f-a93f-08e995ed8006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 74,
                "offset": -1,
                "shift": 21,
                "w": 26,
                "x": 675,
                "y": 154
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3f6a49aa-0372-42a3-b8f0-d70ec4b68ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 74,
                "offset": 2,
                "shift": 40,
                "w": 34,
                "x": 565,
                "y": 78
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0a0e3498-96f1-4070-bcff-2c19e52ec4a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 31,
                "x": 418,
                "y": 154
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9307f34a-6936-4740-a497-dc960d4e7d44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 74,
                "offset": 4,
                "shift": 17,
                "w": 9,
                "x": 135,
                "y": 230
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "033dcff8-c7fa-4bea-af1c-20d3b6b9fe91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 74,
                "offset": -5,
                "shift": 17,
                "w": 18,
                "x": 939,
                "y": 154
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b6675391-f83d-4175-9a0e-de90b93e431e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 74,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 615,
                "y": 154
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "daeb3008-6567-44e3-8243-de5949f29b58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 74,
                "offset": 4,
                "shift": 17,
                "w": 9,
                "x": 124,
                "y": 230
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c126afa5-1700-4041-821d-30d741f6b609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 74,
                "offset": 3,
                "shift": 57,
                "w": 50,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "bb51f830-7ef6-471a-918f-b6dc42d82865",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 31,
                "x": 385,
                "y": 154
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "655e26da-b9af-49c7-bfce-a9cc31c71e6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 74,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 238,
                "y": 78
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "87be9110-3bce-4894-9040-cff0dbea550d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 74,
                "offset": 4,
                "shift": 40,
                "w": 34,
                "x": 457,
                "y": 78
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "dd40a3cd-582a-4cf5-a6ac-b9a238af2dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 74,
                "offset": 2,
                "shift": 40,
                "w": 34,
                "x": 421,
                "y": 78
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c66701ca-b526-455f-b4cc-aaa0b8a99b49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 74,
                "offset": 4,
                "shift": 28,
                "w": 24,
                "x": 730,
                "y": 154
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "76a3a2a6-0c83-42d8-8187-b8eaffafed8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 74,
                "offset": 2,
                "shift": 35,
                "w": 30,
                "x": 583,
                "y": 154
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "23c06402-2c40-45ef-923a-0c13c5630048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 74,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 756,
                "y": 154
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e6b354b1-d98a-4d99-bd65-f32e6db2503a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 31,
                "x": 517,
                "y": 154
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7927adcc-49d0-43ca-8972-bf56dd708a28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 74,
                "offset": 1,
                "shift": 35,
                "w": 32,
                "x": 351,
                "y": 154
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d5ad1f67-655f-4dd0-b08d-314921283ca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 74,
                "offset": 1,
                "shift": 52,
                "w": 50,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d8b8b69b-a505-48e3-9537-193970c9e83e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 74,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 484,
                "y": 154
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c0cf1089-8705-4278-98fd-8bbb3f201cd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 74,
                "offset": 0,
                "shift": 35,
                "w": 33,
                "x": 887,
                "y": 78
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c7cc5e9c-ff8b-4da8-823d-fff00c145088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 74,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 852,
                "y": 78
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "513c8736-ce1e-472e-9645-31f6ddacbed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 74,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 807,
                "y": 154
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "2aeebc13-8e2a-4a32-be3b-66f831cc260e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 74,
                "offset": 5,
                "shift": 18,
                "w": 8,
                "x": 146,
                "y": 230
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ded13125-8b44-45d8-a06a-6f312e6589c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 74,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 832,
                "y": 154
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2cac2579-6892-4204-b871-b1f057cdae2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 34,
                "x": 673,
                "y": 78
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "520f6ba6-6733-4276-bb1c-a031b9bb31e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 40,
            "second": 74
        },
        {
            "id": "06096057-1a4f-4bba-a342-ca06519b2a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 103
        },
        {
            "id": "0c36ff5a-e414-451d-bf6e-514b516c350c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 40,
            "second": 106
        },
        {
            "id": "a5629bc8-d07d-4fd1-82f1-daa924b2b83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8217
        },
        {
            "id": "db03e4cc-8f37-4856-9e1d-2a48b2fc772b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8217
        },
        {
            "id": "ba582be6-3b24-4a4e-aab4-cec591228297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 67
        },
        {
            "id": "e3e4102c-b208-4e99-8419-da43dc7f3eb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 71
        },
        {
            "id": "cb5ea8c3-01f5-423e-9512-6f2f56eb7416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "d3014e0f-abb9-4193-af84-0cff6b6253be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "ec708067-778c-47fa-ba82-33d75f0adb89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 84
        },
        {
            "id": "be78cd9c-1d0f-41f5-bc06-36bb4998e352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 85
        },
        {
            "id": "ae21646d-a510-4cd5-8113-9d2fd3d97fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "46cc6174-652e-4abf-bef6-83928377450d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "5d2a335d-b9f4-403f-8738-dbcfa7611898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "8d93a1b7-5c90-4263-ac7c-e8673718283a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "10108f75-90cd-448a-8104-2d5516963439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "e35f3316-123c-4796-8b52-e53d1e0df9bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8221
        },
        {
            "id": "5fce3366-ad47-4688-bfb7-afd0e86ba916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 44
        },
        {
            "id": "b4ea1202-b0df-45a0-9dc3-d206b5f38402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 46
        },
        {
            "id": "f2e64166-3058-4e72-a94d-db4c2b301947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 65
        },
        {
            "id": "6f0bcd5c-184a-4fe0-b95c-6a37a25dccba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 85
        },
        {
            "id": "fa25bc09-4c7f-4fbf-90ef-a00fe688cdf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 196
        },
        {
            "id": "a32f7163-ab0b-4b78-b4f0-cfc318d7bad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 197
        },
        {
            "id": "cf894b9d-f4dc-4336-a3e9-33c8e1e67821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 220
        },
        {
            "id": "9c22d80d-cd13-4d42-a87a-debd4236aa1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 44
        },
        {
            "id": "1467353c-39fe-4a76-bfb5-3472aa5a7911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 46
        },
        {
            "id": "809af982-941c-40e5-a5dd-e1b5b8e2a031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 44
        },
        {
            "id": "c2e429d9-b050-436b-9e01-5bd5e7c69887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 68,
            "second": 46
        },
        {
            "id": "045c9b99-ec97-42c7-b5e6-d128b3e03283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 65
        },
        {
            "id": "766bc14e-b064-4e8e-b250-327dd2364c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 86
        },
        {
            "id": "b67514d4-a9ea-49b2-b845-59a06dda2393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 87
        },
        {
            "id": "46d5ee95-64de-402a-954d-1f81de3db2c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 89
        },
        {
            "id": "5f4cc571-013c-4966-8d14-e009732427b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 196
        },
        {
            "id": "cd598906-89a3-4136-9353-f68718b2edac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 197
        },
        {
            "id": "334d21c0-3431-48a8-a64f-9799a9665534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 44
        },
        {
            "id": "7f4fd1b4-af6b-445f-b724-8180350eecc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 46
        },
        {
            "id": "1c37630b-2997-41ef-a254-a441e824ab23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 65
        },
        {
            "id": "16cb2b47-dc0e-4a36-a1b5-cafc1d686588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 97
        },
        {
            "id": "515c3f1d-41ff-4639-8c27-90881f9dc309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 101
        },
        {
            "id": "594d5cff-9fdc-4197-ae5e-7dc5d4adbbec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 105
        },
        {
            "id": "a855b8d7-9f01-486e-96a9-e0100f394f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 108
        },
        {
            "id": "c4113477-8807-4ad6-845c-7f2027a9b397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 111
        },
        {
            "id": "1e45868c-1385-4b71-be62-0f99c6664a03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 114
        },
        {
            "id": "ff11fd01-8f93-4ddd-b693-620e29e0c01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 196
        },
        {
            "id": "ef4125c0-6553-4a83-8854-41e6caf36ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 197
        },
        {
            "id": "570b88bc-fda8-4c4e-a654-839887443349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 228
        },
        {
            "id": "e80249ca-30d2-49e3-9ab6-145b19e42f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 229
        },
        {
            "id": "f15fa362-e836-4b82-9b3d-0ebc1a005d2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 246
        },
        {
            "id": "d7ae1451-3d00-46fb-9cb7-695832d9419b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 44
        },
        {
            "id": "51150512-26b5-4876-b127-3a0719a4c52c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 46
        },
        {
            "id": "dbc15d1c-2ff6-4dca-a994-1360d31e73cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 65
        },
        {
            "id": "5f1b9f6a-0704-4bc1-a77e-17fdced53249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 71
        },
        {
            "id": "7e2f24c1-ca47-4c87-ab21-90d7d26068f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 74,
            "second": 44
        },
        {
            "id": "e96c24ff-3bb0-4b49-9b86-e18cb9edc895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 74,
            "second": 46
        },
        {
            "id": "0bfa67f1-f9a6-499b-9c8b-b31012c77859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 58
        },
        {
            "id": "de8929e0-4502-4ef0-97b5-4ab1a8b6f995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 59
        },
        {
            "id": "e7dfd855-cb6e-4bc1-80a3-3659915f296b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 65
        },
        {
            "id": "3e6556b1-ee71-4e6e-acf6-76c08c4990ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 196
        },
        {
            "id": "c9faf0b3-8721-482b-8bcb-b829dd26dd3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 197
        },
        {
            "id": "c96a9289-e004-4740-8f84-8490a8cee8e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 41
        },
        {
            "id": "6326d24b-1a63-48ab-bd9f-77080ce15d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 67
        },
        {
            "id": "f5efd71f-58ae-4f14-a8b8-80252e0efb12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 71
        },
        {
            "id": "fa3c649b-2786-49f9-9bff-d579c20adc91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 79
        },
        {
            "id": "1606c5e9-5c08-4a91-8219-0d32c8e84d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 97
        },
        {
            "id": "2601a2b1-696f-4aae-89b0-eaee32e81a32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 101
        },
        {
            "id": "4bfd3758-972f-48e6-818c-d553a36dcc65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 111
        },
        {
            "id": "9146c829-fda0-4927-a736-bee0f2dd1fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 117
        },
        {
            "id": "05dce805-4917-4f12-accd-4b3437b60c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 118
        },
        {
            "id": "cfc745cc-ccec-4db9-8705-4bfc689bfaf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 119
        },
        {
            "id": "dbb5e7bf-6e21-4d1f-8260-f52b0717947a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 121
        },
        {
            "id": "7f01ce5b-9c52-46b7-965d-8ba17ab1864b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 214
        },
        {
            "id": "c6b081e5-f1a4-4298-a7d4-0344f06233dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 228
        },
        {
            "id": "eb61357c-6290-4bfe-ad65-2331234430b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 229
        },
        {
            "id": "5bc54ca4-5480-4e9d-a89e-c18c69c726c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 246
        },
        {
            "id": "c2c4b76c-994a-483a-8524-4235b95fe4be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 252
        },
        {
            "id": "ed925a84-0b34-48a2-9716-5022c6762ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 65
        },
        {
            "id": "4a397e48-c6cb-4362-adcb-9b8368e7ca3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 67
        },
        {
            "id": "14742066-f3d0-49a3-b9e0-18270aa922c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 71
        },
        {
            "id": "5ec54f1f-fdc2-4bb9-8c92-cef28ae10177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 79
        },
        {
            "id": "4347a139-d246-44e1-a035-cd377ce58d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 83
        },
        {
            "id": "2ce6ee6e-a99d-4ff0-87e1-fb71174ef12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 84
        },
        {
            "id": "fe049977-d5b9-4ffb-b715-185f3207d806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 85
        },
        {
            "id": "02f9ae0b-52b6-4199-b3cb-a57b555f6d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 86
        },
        {
            "id": "907cfd97-dbb7-4bd1-a081-c10063c82019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 87
        },
        {
            "id": "28ab843f-f208-4b1f-b01a-3a93783f90d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 89
        },
        {
            "id": "2ae887df-a847-4936-b129-81897a3e6fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 119
        },
        {
            "id": "43c67016-4bf0-4c3c-aab4-bff6a5b41625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "b04bd805-8fe5-446c-a7fc-3a3b3410569c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 214
        },
        {
            "id": "e3b49439-b25d-4d4d-baf5-b19fb2f3aab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 220
        },
        {
            "id": "46b41adc-c4e9-4b0e-bbc3-2224c7f5e8a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 8217
        },
        {
            "id": "973348fc-5b22-4c27-a3f9-f350b960fb8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8221
        },
        {
            "id": "b6125933-34c8-4736-b25e-2d6c236f6f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 44
        },
        {
            "id": "572d7d8d-9a7f-4902-a01c-b9cffee0ec24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 46
        },
        {
            "id": "521618d4-7925-4207-ae35-66fde4e33012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 65
        },
        {
            "id": "c45a87ad-294b-4a4c-aa0d-6bc61ab7428e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 196
        },
        {
            "id": "5b1d51f3-70de-4550-afe2-197eb7b247a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 197
        },
        {
            "id": "08f6de01-805f-4825-92e0-cf1c8c1c134a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 44
        },
        {
            "id": "eee3fa95-551d-4625-85d4-4dabb937a906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 46
        },
        {
            "id": "33f3be6d-396a-4179-83a0-f3d91e62dd54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 65
        },
        {
            "id": "9fea2b60-e5b4-4d5d-9a22-2ec1309b7683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 84
        },
        {
            "id": "7022178f-1697-456a-8b91-319cfdb1acd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 86
        },
        {
            "id": "3f8218f6-b8e3-46a5-9235-9ea4828b6c49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 87
        },
        {
            "id": "e8ea99ab-e48d-4c68-8e64-adaec940b8fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 88
        },
        {
            "id": "383038c0-700a-41ea-9928-322a46f1b6df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 89
        },
        {
            "id": "bd1a3a39-f84a-43c2-b577-60f2d03dbc2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 196
        },
        {
            "id": "e678283f-4216-46ce-91ee-3b3327ebb58e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 197
        },
        {
            "id": "0ce4ce03-69b5-48dc-8d5c-72411a8669dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 44
        },
        {
            "id": "3366942c-f765-4631-8d89-d26eb070ef6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 80,
            "second": 46
        },
        {
            "id": "a8c73d23-12c9-4aa0-97cd-076c93bf28c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 65
        },
        {
            "id": "97e08043-f7ab-4725-b28e-eecf86125d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 97
        },
        {
            "id": "9893d069-b5c2-41eb-bab0-b8735b3cde56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 101
        },
        {
            "id": "78bf03a9-607b-4fdc-9f93-96280434db2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 111
        },
        {
            "id": "f9a8050a-4e08-453b-9ad9-ba5a0ff0a490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 196
        },
        {
            "id": "7b7d1df8-9dbb-4665-aaac-816e24cbe1a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 197
        },
        {
            "id": "454f19eb-f145-45c4-b5b5-5b41e6282c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 229
        },
        {
            "id": "cebd0530-750d-4d39-a0d2-838de5db6197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 84
        },
        {
            "id": "465efccb-61d1-45fc-a62d-d41c491d7532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "b7fc2ca5-acfd-495b-89b9-71c1a999fb26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 87
        },
        {
            "id": "66227993-c2e5-4113-b14b-2ca247d9903e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 89
        },
        {
            "id": "63548995-b65c-4d22-bcb5-731d8b882782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 102
        },
        {
            "id": "1953341d-1a49-4b15-be31-54dd0e88fdff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 103
        },
        {
            "id": "eb659e30-738e-4828-9b77-b778c0a90719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 81,
            "second": 106
        },
        {
            "id": "4cb1ce26-5e3d-463f-bd67-9ed043302d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 112
        },
        {
            "id": "dce6b3e8-a6f7-4df4-b56a-8b2023af0d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 121
        },
        {
            "id": "2bb455e4-8096-47cc-81d6-d6e34d0c9a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 41
        },
        {
            "id": "05f46951-f377-492c-a923-b1e31e3aab5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 67
        },
        {
            "id": "6105b3fb-b0e0-4837-9af9-695345451d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 71
        },
        {
            "id": "feb9a640-7cd7-4499-a266-9ffabfb3b74a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 84
        },
        {
            "id": "9c942b0a-b4fd-453f-9d74-ce908ac34efc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 85
        },
        {
            "id": "e398f60f-9578-4a25-acf7-988f4d6d4d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 86
        },
        {
            "id": "9ecf317e-f90c-469d-89f8-e2e0bbc8b33a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "e283e5cd-0377-4c8d-a2ac-94a4c6c2ca2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 97
        },
        {
            "id": "b50e676b-c356-4d7e-a7cd-20f769ff675a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 101
        },
        {
            "id": "e43a53bb-21f1-4bd4-8138-0b3b8aefef35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 111
        },
        {
            "id": "cbce46aa-325c-4900-8130-b4850bffe9b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 117
        },
        {
            "id": "b4c0070f-f14c-4939-a4cd-f0f0e6e06893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 118
        },
        {
            "id": "7403e10c-ec7f-4d3e-bb86-16f90d39e65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 119
        },
        {
            "id": "9f14cc6f-7289-41f7-849b-80a6868fb2b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 121
        },
        {
            "id": "0816fb8f-d208-400b-a20e-f8ca699de944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 220
        },
        {
            "id": "8cad6dac-05e7-48c9-b961-5a8dc23f6e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 228
        },
        {
            "id": "df7783ea-dabc-4ae2-9a8f-d16d7fd012ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 229
        },
        {
            "id": "0a79caf1-5944-4b18-a395-7dcac5654c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 246
        },
        {
            "id": "5ae89449-e575-4639-8c83-a8b9ff943ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 252
        },
        {
            "id": "915ffe7f-a4b4-482c-8929-653595805169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 44
        },
        {
            "id": "b022ee89-618d-4070-aa9b-878ab0a3af44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 46
        },
        {
            "id": "ecd9bb82-4070-4c8c-a984-cde0b886d403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 44
        },
        {
            "id": "3a91ce27-115b-4dc5-a617-45c3efc8a351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "e38dc267-f118-4e0b-946b-832a5e1a54f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 46
        },
        {
            "id": "01e9aa2d-f618-425c-8d6b-774a29ec6834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "301bf033-d9c3-473f-b340-20a98523543e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "5e171d9b-6f7b-446f-a333-5e5ca31e24b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "26cc46a1-6b24-4eef-8a7c-7d4358b03fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 67
        },
        {
            "id": "2eb7e196-66db-4dc8-b92f-cd39584574d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 71
        },
        {
            "id": "ae2f7f1b-c4e2-4462-9a47-97879e1db5c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "156d9aac-5e63-4fdc-b751-23244ef839d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "0c87e61c-890a-4445-bcb6-c92d8f4231d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "a32d5481-9a73-4293-9588-4849e01e1294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "1e2fdde5-67dc-46c7-9222-a5989a0e1a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "26f2b293-28b5-4844-b5c4-cc9f2278f63b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 104
        },
        {
            "id": "3034eedb-21a5-4278-84e3-e37642030845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "3ce57f43-949e-4611-a125-d8d95e702e7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "cd7c906b-f21d-453c-903d-112e1c69fed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "6f2978d2-c53e-4ebe-a293-9e18611dbd69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "4d62454f-615d-4ea3-958a-75a6b1f2830c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "5296d4b8-2b2a-400f-9270-5f37c165cd60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 117
        },
        {
            "id": "ce9b9518-569c-4182-b73e-3ee79f113d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "c8e4f86a-1268-4a80-a8ec-7ad202e2a280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "db04826a-a4de-49a0-9574-efb248bfa0c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 122
        },
        {
            "id": "e4f777b7-5aea-4460-9003-74bebb832475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "eb363bfe-37d7-4aaa-9491-b290267cb945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 196
        },
        {
            "id": "c2ec8fb0-25c3-4e86-aae8-58dd3139f07e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 197
        },
        {
            "id": "baf0d48a-9396-4d25-93a4-ff1857a7b6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 214
        },
        {
            "id": "21b49f80-fa4e-40c4-b667-dab54b4dc8ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 229
        },
        {
            "id": "4a997d2e-781a-40cc-9968-1cf46862abb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 44
        },
        {
            "id": "2493c257-bbd6-4a9f-80d2-2e5c882f8214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 46
        },
        {
            "id": "5f34bd60-2a97-491f-b3b4-659f6eae46e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 65
        },
        {
            "id": "a0ba7965-d7e5-49fb-9c13-9b746be7cddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 196
        },
        {
            "id": "da523262-c82f-4bb1-8b27-5f01af474b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 197
        },
        {
            "id": "f647a123-abc8-4b1e-809d-8c42519e39fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 44
        },
        {
            "id": "f7aba4c6-c254-4a20-9ab9-6ed362cddb66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 45
        },
        {
            "id": "915421e6-5163-4fa9-92cc-389db9ceae36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 46
        },
        {
            "id": "d268a639-8e4a-4df1-bb28-11dbff0a198b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "c607627a-4b92-4b06-887b-9cf4164a7d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "22b76302-2a20-4995-82e7-5a1778a99a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "4be0a4ff-662a-4af7-a436-e25316d7b7a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "807f7294-05de-45ac-b91c-87fd86867138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "85dff48e-04b5-4efa-8a50-bc3c5f97155f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 79
        },
        {
            "id": "7c061a08-d086-48d5-8056-d614f7b7ddac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 81
        },
        {
            "id": "2016c1b8-a0b6-42ec-8970-bd95710a1bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "be9fc5ba-2aa5-4387-a507-85181dce9c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "3d864405-0956-4c1c-835d-ec6c43dad57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "08e55f3e-24a1-4c6d-8ebf-1fd4b96b7dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "ea66badb-a604-4a6b-9e9c-2cbdf33d0f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "dc56c40c-714c-4c04-8208-c5d11acb58b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "1cbd9307-237c-4c25-bd80-66da03e51497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "d22b7946-dbd8-49f5-9c95-39328afc37e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 173
        },
        {
            "id": "cabb8e78-6858-4b2e-8f8c-ef2e70a12ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 196
        },
        {
            "id": "d5fdd968-145c-4850-89a2-1ab0f53e169f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 197
        },
        {
            "id": "b598a787-a5fc-4f0a-95f2-b9f26b46c618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 214
        },
        {
            "id": "8093a62d-196a-412b-a9ec-564c3b9b22b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 229
        },
        {
            "id": "ef5d5541-385f-4766-a67e-b45bce8c6540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "c59fc7df-488f-4f5f-b09e-3b0c8af5719f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "f225532c-6144-4431-9583-64335a067f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 46
        },
        {
            "id": "b67ab400-ced9-436a-8f08-fc169783ae52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 58
        },
        {
            "id": "9bb4cd4f-5634-4f52-b3a7-5556e54fa818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 59
        },
        {
            "id": "72a41052-3b68-46a8-bc61-86ca76c00705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 79
        },
        {
            "id": "3324c9da-f07a-4774-9080-ae5330073622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "ee65854a-9830-4830-92d7-8d4cda984589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 100
        },
        {
            "id": "24a7060b-47e3-4e00-8704-d3ea40df4ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 101
        },
        {
            "id": "1a6f1d64-64bb-458e-8994-39df6d25d9ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 111
        },
        {
            "id": "94dbf66f-7e00-4499-8963-25cfd8ff0018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "df5a55d8-d631-47bb-b56c-dfb1c611cada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 117
        },
        {
            "id": "bc9d9d80-3fe8-4590-a244-1beea305fe46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 121
        },
        {
            "id": "1c4e2d12-90cc-4e90-89ef-2282ebcfc929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "604d2c86-b31e-45c9-bd34-73281b8b18c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 214
        },
        {
            "id": "7ba7015b-8512-4b82-9df7-088e7d4a02c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 229
        },
        {
            "id": "1d554792-6f13-45b7-a1e6-102630b6c89b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 67
        },
        {
            "id": "fb7705e1-f283-41e8-93f1-1bb35be2b865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 71
        },
        {
            "id": "7a82039c-3008-4939-bdea-b9915881492d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "a98f205f-c361-483f-9b00-12b10eb03114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 214
        },
        {
            "id": "7875fa9a-3af5-48f0-b3e1-ceca3401c30e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 44
        },
        {
            "id": "57f7b5d4-3fc5-4ae8-b5fe-85166bab637f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "47339d64-01e2-4f18-8ac8-09a7baa0b5ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 46
        },
        {
            "id": "385ea6f6-1508-4941-9d11-6aea2c264308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "6694abd6-6f79-4c7e-83c2-4d9341455142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "a5bb5e4e-f849-4964-84db-bef63e52f0b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "f63580e6-1e56-4324-8609-8b7b8fe14ceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 67
        },
        {
            "id": "2588ee0a-bd9b-4ca9-b2ca-c577bda0722e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 71
        },
        {
            "id": "4f54bd14-c503-4f57-829d-2c6c12ac6e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "7cb17e54-ea57-46eb-95e5-f569222b35b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 83
        },
        {
            "id": "71e0683e-6c6a-4480-8dd4-7d78cfa16cc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "6cd27119-0417-4012-94b4-641c0a4d77e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 100
        },
        {
            "id": "24fec6df-742a-4201-aafa-d9bee6a392ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 101
        },
        {
            "id": "d8abc348-b251-45a4-8314-90e1d8062949",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "9e501054-1bce-4a7b-a9bc-92f3dd952ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "e67e899f-9362-4075-b89a-e90ff18bf252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 112
        },
        {
            "id": "5435438f-104e-465c-bb7e-93e21eca1bbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 113
        },
        {
            "id": "68c25864-cf81-4ed1-a02f-96b7de3a5c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "eb55f961-3690-4f49-820b-ecd13311b614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "e80aea39-9f93-481f-b9fb-5b1f7e9a1ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 173
        },
        {
            "id": "81467810-a123-46cd-b416-31562171779c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 196
        },
        {
            "id": "3ddd3f94-9b06-46a8-9652-1536aab843a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 197
        },
        {
            "id": "a0d0ad17-6a6b-4be6-a2fe-04a400d9d269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "22b92914-087f-4f1c-ae16-d71177928630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 229
        },
        {
            "id": "02c09684-2dce-4882-9c0e-fc080c9a2a93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 98
        },
        {
            "id": "c2416c31-e037-4d63-b08c-11a93810e0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 103
        },
        {
            "id": "97bd3dff-25c4-4d1b-b1a4-5b22dab1207a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 112
        },
        {
            "id": "ff49b850-d8a6-473f-aab4-f0c891411a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 116
        },
        {
            "id": "2c914ac2-bb31-4cd8-8e61-c283a52a970f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 118
        },
        {
            "id": "bb51a812-f3b5-417c-a015-75ae29af9a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 119
        },
        {
            "id": "2488aed5-4c05-4ce4-b892-157e661b51e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 121
        },
        {
            "id": "f88737a6-cd12-49a3-93bc-b17bfd6e639d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 44
        },
        {
            "id": "d6d08533-4172-4986-9858-75bf54491086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 46
        },
        {
            "id": "222a940c-30ca-4ad4-81b4-2e85ca669295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 98
        },
        {
            "id": "a8ba28d4-b296-4489-9d04-bd6910947b38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 108
        },
        {
            "id": "796ee95b-8fb5-4727-a05d-e5e21121235e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 117
        },
        {
            "id": "9de455ae-699f-478e-9f22-f6b948382826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "ad94cf4e-61f5-41a4-85aa-d15a49beee06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "5253cb99-8c2d-40cc-aa9d-cb1810fa518e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 121
        },
        {
            "id": "0cf9efc8-e86e-4528-a0ed-d23f003f8350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 252
        },
        {
            "id": "67934207-cd1a-4c6c-b8e2-1aedc261446d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 44
        },
        {
            "id": "469832fc-7076-4ba3-88a1-624975396d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 46
        },
        {
            "id": "0278738b-4637-4457-9cbc-0548cdbf4cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 104
        },
        {
            "id": "16e72b18-125b-47f1-9bae-625ce96c5350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 107
        },
        {
            "id": "de3b3b3c-e36c-4f30-8196-22d27b3f3cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 108
        },
        {
            "id": "5d12c774-f57a-4e04-99e6-20d4a000ab7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 121
        },
        {
            "id": "6deb0a19-ae8d-4f12-9d91-01d89697e1d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 118
        },
        {
            "id": "fcb48a1f-cc61-4d30-8705-e5fcb6ed0a32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 8217
        },
        {
            "id": "f87063b1-7f9f-47da-9a40-bd146c57f344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 44
        },
        {
            "id": "d5947aca-78f1-4c13-ac2a-dc6dc7cf9232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 46
        },
        {
            "id": "c27b6a57-9cca-4426-8c49-5c3800776ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 98
        },
        {
            "id": "19dd5f0e-3ed0-41f8-a922-fb94c4a245ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 103
        },
        {
            "id": "5a56f4d1-ede0-4967-817b-6ea04d82c294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 112
        },
        {
            "id": "7bf01d4b-2810-40a4-96a5-caede952f2cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 118
        },
        {
            "id": "566e35a9-6a1e-4830-957c-ba93d10a7c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "14b012e7-3d0c-4b34-a58d-d01200176ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "67e6337c-83da-426b-a567-fe7f741dc26d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 121
        },
        {
            "id": "4bf32a9c-99e5-4fab-b93e-c8d9e9b60a56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 122
        },
        {
            "id": "8e7af5ee-5a49-44c4-9842-40de54696cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 102,
            "second": 32
        },
        {
            "id": "32dc8ce1-cc1a-421c-8e12-3b86e8dcda8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 33
        },
        {
            "id": "32af9ffa-bbe4-4624-8131-149443a00580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 41
        },
        {
            "id": "88b5ff0f-b925-4037-b449-bb6c84cd7f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "ee471125-977c-4c2a-9068-d31ccf829413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 46
        },
        {
            "id": "2cb81162-d8c5-4f68-980a-73a02802d718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 63
        },
        {
            "id": "234b44b1-4a72-408d-98b4-76f0810a468f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 97
        },
        {
            "id": "21d5a47d-429b-45c1-b142-dd3de1fd3181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 101
        },
        {
            "id": "5b3b82b0-2720-45f3-8259-e4a8718b3189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 102
        },
        {
            "id": "13b5a0c7-dd46-4a83-bd46-6671ef07bf76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 105
        },
        {
            "id": "903eaa7d-0383-4688-8d96-9defbde15f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 108
        },
        {
            "id": "b191c99f-ce39-48d1-8e5d-118fe4edf082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 111
        },
        {
            "id": "8cf72eed-d66c-45ea-ae5b-9b0dc44b4ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 228
        },
        {
            "id": "d06c221d-a7e7-4ba2-af4e-ab63edae7e7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 229
        },
        {
            "id": "3c135789-2608-4aa3-875c-45f9d41be24f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 235
        },
        {
            "id": "c2857140-772a-4524-9aae-01ba5711a41c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 102,
            "second": 239
        },
        {
            "id": "b61aea34-cbe2-487e-92fc-1f2ceb8663a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 246
        },
        {
            "id": "bd8a4179-08f5-4abf-8b55-379e6f58b3c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8217
        },
        {
            "id": "d333726f-de5e-41db-bcd6-17b6029e2535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 8221
        },
        {
            "id": "5f1ee34e-826f-46e0-845a-47053c0b0950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 44
        },
        {
            "id": "43b0d7b8-5faf-4d17-8224-e7f55e8749ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 46
        },
        {
            "id": "0908ff92-ca7a-4e79-9ef7-3a71acf1b70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 97
        },
        {
            "id": "45af1572-59e0-4873-9f18-6ad70e636c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 101
        },
        {
            "id": "9cb2c4dc-ad75-4db3-a702-ea7a20e877a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 103
        },
        {
            "id": "94b2112a-afbc-468e-9d43-ad8f5dc40059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 108
        },
        {
            "id": "39f2fcbe-accf-4b86-b26b-0eb592d30c7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 111
        },
        {
            "id": "2bc43a8d-5e6c-4e40-b56d-e41e276f580a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 112
        },
        {
            "id": "75a7da43-1478-4041-a057-a7abab63b061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 114
        },
        {
            "id": "b3de7916-2afe-4aaa-87c8-fc57d0234387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 228
        },
        {
            "id": "61fc18f1-0abf-4123-ba9a-b70a3453d90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 229
        },
        {
            "id": "a2ee0f79-71cd-48e8-9650-93f94915a430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 246
        },
        {
            "id": "91001ba4-d2c7-4af5-8c33-536c4cd2150e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 97
        },
        {
            "id": "a3e2c6ff-a942-4018-9dc4-384c74e02a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 99
        },
        {
            "id": "493e622c-1ed9-4e69-afa5-c44d527fe4c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 100
        },
        {
            "id": "372bbc3e-b16a-4390-9ff6-167313355722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 101
        },
        {
            "id": "7d3d2922-ea70-4394-91fc-3c680d746768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 103
        },
        {
            "id": "c39d7ccc-558b-4c89-b229-4807052352bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 105
        },
        {
            "id": "9b2f52d4-7841-4acc-a2e0-2d43a2a9b965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 108
        },
        {
            "id": "c8b8b5a9-630e-46ab-8a1b-311df17ff882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 111
        },
        {
            "id": "bd69ab1c-def6-46ea-8143-1afedd3666ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 112
        },
        {
            "id": "526f7da1-fbcb-4f6b-9738-090c29261aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 113
        },
        {
            "id": "4233ff33-7b60-409c-9852-50dcc78f19c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 115
        },
        {
            "id": "4cfbb3aa-299d-47b6-ae0a-e34216190ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 117
        },
        {
            "id": "7f138708-06af-422e-88fc-362cd999e729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 121
        },
        {
            "id": "e1951231-f60d-4164-a49b-9d64304591e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 228
        },
        {
            "id": "29af42e3-a43a-41a2-923e-9a5a2678f402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 246
        },
        {
            "id": "05903117-cfa8-46d6-8a00-13f0589c6f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 252
        },
        {
            "id": "375d8946-7994-4a21-b9b0-b9c9dce19b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 119
        },
        {
            "id": "b21891e8-3c99-4db7-91f7-c6b0b1302425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 117
        },
        {
            "id": "68f3b987-1696-43fc-9385-f0a19a4d08ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 252
        },
        {
            "id": "eb63e10f-57b7-41cd-8060-ec3875a7ca49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 117
        },
        {
            "id": "5b099246-01b1-42fc-82b7-ae4afc1edd82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 118
        },
        {
            "id": "a51ebf4d-a79f-4172-8bdc-eeb29eeba3df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 252
        },
        {
            "id": "092a26cc-f4bb-40bc-a68a-287e119d36ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 44
        },
        {
            "id": "c374a0f2-ee61-41fd-a54c-09313cf0d990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 46
        },
        {
            "id": "8ae6dbbe-3098-4e09-b8da-3f39b49479e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 118
        },
        {
            "id": "a6eba338-2b34-4c9e-a7ba-5e847ee31c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 120
        },
        {
            "id": "69e30bf7-2e5e-4aba-8a6e-62a9c95170af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 121
        },
        {
            "id": "147b178c-12d2-46fb-9d33-37a6c92a45fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 44
        },
        {
            "id": "9be828ba-d14f-4f47-ba3a-b4f5b37a9549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 46
        },
        {
            "id": "d2a7c1c8-0167-4152-8fb9-0964477e5133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "9c59db00-2506-42f9-bb6f-196938738dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "530982dd-328a-46d3-bb00-e49916eea805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 122
        },
        {
            "id": "24e6a80b-b22c-423f-88c0-b34c8793d4b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 114,
            "second": 44
        },
        {
            "id": "4afcda86-dcab-4d13-8343-49d09355935e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 45
        },
        {
            "id": "6e735786-73e2-4f76-8ffe-1e3aa4cbfcf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 114,
            "second": 46
        },
        {
            "id": "6ac287b2-fb8d-4cc4-be2e-2b21afd0a02d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 58
        },
        {
            "id": "941e5f67-4dd1-4e15-a0e7-20712cade782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 59
        },
        {
            "id": "2a8d1f41-e27d-422a-9fe1-650631893b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 97
        },
        {
            "id": "9aac74c9-1de4-403f-b82a-e69ce25bf09b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 99
        },
        {
            "id": "4f38c47e-51c6-4764-b6de-48538cfc8002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 100
        },
        {
            "id": "f51bc6a0-a9dc-493b-81e2-a8e9d1334922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 101
        },
        {
            "id": "194b8501-50e9-42b3-a494-043ce2e7cba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 103
        },
        {
            "id": "8a29a432-43c7-417c-bf9e-434125a5f36b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 106
        },
        {
            "id": "8c76bd37-3da3-4a9f-ad71-21444da92544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 107
        },
        {
            "id": "c5c80d4a-4331-41a8-9493-10f3faa563d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 108
        },
        {
            "id": "7cb48b5c-330b-451d-a29c-a9bf7a7e6a32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 111
        },
        {
            "id": "66fbb3ce-6414-4c5c-82a8-3f1649feb15d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 112
        },
        {
            "id": "01587f7e-9dff-4f23-9f80-9677c90409ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 113
        },
        {
            "id": "0829fca4-f17a-4da0-9673-2ece57388c9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 114
        },
        {
            "id": "cd973e0d-abcc-477e-b54b-5fd8c869917f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 115
        },
        {
            "id": "9d0b9e22-c54c-42e0-89bb-9ca7e542b53f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "05097374-516c-4814-bcc5-ed5f95fdbb9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 117
        },
        {
            "id": "2f17377c-e52a-4912-bda1-f656a78318a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 118
        },
        {
            "id": "f49e0c52-14e7-4985-9e62-41d51660ad08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 173
        },
        {
            "id": "99fef766-7efe-4e69-ab57-fd410a91817a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 228
        },
        {
            "id": "bd35ad83-0127-4276-8729-6586c51b2d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 229
        },
        {
            "id": "c79ed97b-387e-44db-a7f7-a5cc2b95d55e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 246
        },
        {
            "id": "e373260d-8e24-4c87-b8ef-8a8562fff2eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 252
        },
        {
            "id": "9b655d6c-845f-4c48-ac7d-809f3c885485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 44
        },
        {
            "id": "f49fa8b5-a551-4937-b884-8ec9dd7c35cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 46
        },
        {
            "id": "4af8d448-703d-4768-8b84-ee0b98bef47c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 119
        },
        {
            "id": "0529be31-4a82-45c9-b050-825cef64e077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 116,
            "second": 8217
        },
        {
            "id": "d475128f-aae1-4938-9427-422d3c773487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "1077ae72-c9d8-423b-84b8-6b5a8df35c7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 46
        },
        {
            "id": "076aea93-fbee-4d17-9d75-b77e856a1a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 97
        },
        {
            "id": "33beeb60-cc69-42b9-8cc7-22c2af1f6550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 99
        },
        {
            "id": "5d585ff5-2548-4a68-bce6-c5fea81918bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 100
        },
        {
            "id": "12211633-793e-4432-adb3-c54272581b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 101
        },
        {
            "id": "4c3cd35a-03f9-4571-823d-8f4f83fa43ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 111
        },
        {
            "id": "c5889d49-253b-487a-a207-36c786db9027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 113
        },
        {
            "id": "a1ddd28c-8e34-420c-9b62-a6ce76e69bde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 228
        },
        {
            "id": "13423bbf-5310-4f4b-b8d2-40071b9804d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 229
        },
        {
            "id": "a17c54ab-64da-4a84-935d-1ffb545e7de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 246
        },
        {
            "id": "0f9d2111-4f8b-4f20-82ec-02a337f0699d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 119,
            "second": 44
        },
        {
            "id": "07da3c22-e7e9-438a-aabf-9dbc0f60af7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 119,
            "second": 46
        },
        {
            "id": "ce41ec3d-70b1-4a87-9e3c-499b1e8ae3f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 97
        },
        {
            "id": "cc03822b-b129-4129-b0f7-f22bc64992b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 99
        },
        {
            "id": "05e5e11e-4711-4485-8eaa-9c37f5583c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 100
        },
        {
            "id": "51a4f03f-f70f-4754-b1eb-2787548c1fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 101
        },
        {
            "id": "0ae2d719-e159-42f5-b656-1e78b98fe77a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 111
        },
        {
            "id": "b3e9b878-a489-4a4c-8f0c-67365eb8870b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 113
        },
        {
            "id": "98608e0f-755f-48b4-b766-2972510cc43a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 228
        },
        {
            "id": "ab86779c-8af9-4371-af76-3fea927c3730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 229
        },
        {
            "id": "6b29e7dc-c314-4b6e-b816-c839470bd3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 246
        },
        {
            "id": "341db405-c644-4b03-9bea-e83bed76f9dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 99
        },
        {
            "id": "54d371e8-7241-412a-bcd1-5b7b69a97c42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "83e3a672-c63b-4f2c-a873-283fbbb34f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "cbc53dea-bf66-4771-96d7-8b646f867063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "052c7152-9fe7-4ef6-9fb5-5c67182462c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "e9c0542c-a839-4664-b2d8-a022efb6496f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 44
        },
        {
            "id": "56882558-2962-4570-a633-f8a84de93fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 121,
            "second": 46
        },
        {
            "id": "1f33d40a-6fc4-4d99-a2bc-3224953a2357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 97
        },
        {
            "id": "cd4f659e-4b37-41e7-96a4-58f5c98ccf46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 99
        },
        {
            "id": "118f3fbe-a28a-4f0e-bbf5-a337062e4980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 100
        },
        {
            "id": "befa4d58-cc23-4b84-b06d-3c0603a01188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 101
        },
        {
            "id": "f1df5fee-02d7-4f1f-b1da-d616cde9144b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 103
        },
        {
            "id": "d5f5231f-a0d8-4eea-9227-e22767976b68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 111
        },
        {
            "id": "8fea5359-8da6-4f68-91f2-4f4e4b108c42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 115
        },
        {
            "id": "b8de58d1-9033-4e71-bc42-4362aadf7b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 228
        },
        {
            "id": "84af1875-4022-4614-9a53-62729b0dedfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 229
        },
        {
            "id": "78effc58-1749-4850-8c38-e28c9a99aa06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 246
        },
        {
            "id": "f714f8f2-f385-4e36-b4ea-3eb9d9468614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 8217
        },
        {
            "id": "5ab8c5e8-0509-408c-940e-e0f5976c951e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 99
        },
        {
            "id": "ba716885-22b1-4b47-8f6d-4793f4325cf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 100
        },
        {
            "id": "94d27f85-41fa-466d-a0c2-12faede97435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 101
        },
        {
            "id": "93137a4e-a86a-466d-a103-80a28cd4dc5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 111
        },
        {
            "id": "f9f66bcf-6394-4d85-b50f-1afa81c92b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 246
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 48,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}